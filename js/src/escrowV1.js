import { ethers } from 'ethers'
import { ethersProvider, currentChain } from './config'
import contracts from './contracts'
import { abi, networks } from '../vendor/DaiERC20'

import util from './util'
const { isEthereumAddress, bytes32Cid, genMid} = util

import eip712 from './eip712'
const { signContractTerms, signMilestoneRelease, signMilestoneRefund } = eip712

function parseAmount(amount) {
    const biAmount = ethers.BigNumber.from(amount)
    if (!biAmount.gt(ethers.BigNumber.from('0'))) throw "invalid milestone amount"
    return biAmount
}

function parsePercents(num1, num2) {
    const biNum1 = ethers.BigNumber.from(num1)
    const biNum2 = ethers.BigNumber.from(num2)
    if (!biNum1.add(biNum2).eq(ethers.BigNumber.from('100'))) throw "invalid claim amount"
    return [biNum1, biNum2]
}

function parseMilestoneIndex(index) {
    if (index < 1) throw "milestone index starts from 1"
    if (index > 9999) throw "milestone index starts ends with 9999"
    return index
}

function parseMilestoneParams(networkId, { paymentToken, treasury, payeeAccount, refundAccount, autoReleasedAt, amount, parentIndex }) {
    if (!isEthereumAddress(paymentToken)) throw "invalid payment token address"
    if (!isEthereumAddress(treasury)) throw "invalid treasury address, use escrow contract address if in doubt"
    if (!isEthereumAddress(payeeAccount)) throw "invalid payee account address"
    if (!isEthereumAddress(refundAccount)) throw "invalid refund account address, use payer address if in doubt"

    if (!parentIndex) parentIndex = 0

    const escrowDisputeManager = contracts.contract('aragonCourtDisputerV1', networkId).address

    const biAmount = parseAmount(amount)
    let biAutoReleasedAt = ethers.BigNumber.from('0')
    if (autoReleasedAt > 0) biAutoReleasedAt = parseAmount(autoReleasedAt)
    return { paymentToken, treasury, payeeAccount, refundAccount, escrowDisputeManager, amount: biAmount, autoReleasedAt: biAutoReleasedAt, parentIndex }
}

function parseCid(cid, error, defaultCid = null) {
    if (!cid && defaultCid) {
        return defaultCid
    }

    if (cid.length != 46) throw error
    return bytes32Cid(cid)
}

function escrowWithSigner(networkId) {
    const escrowContract = contracts.contract('escrowV1', networkId)
    const signer = ethersProvider().getSigner()
    return escrowContract.connect(signer)
}

function ERC20decimals(networkId, tokenAddress) {
    const ERC20 = new ethers.Contract(tokenAddress, abi, ethersProvider())
    return ERC20.decimals()
}

function ERC20symbol(networkId, tokenAddress) {
    const ERC20 = new ethers.Contract(tokenAddress, abi, ethersProvider())
    return ERC20.symbol()
}

function isApprovedERC20Amount(networkId, tokenAddress, wallet, spender, amount) {
    const ERC20 = new ethers.Contract(tokenAddress, abi, ethersProvider())
    if (!isEthereumAddress(wallet)) return Promise.reject("use Ethereum address as wallet")
    if (!isEthereumAddress(spender)) return Promise.reject("use Ethereum address as spender")
    const biAmount = parseAmount(amount)
    return ERC20.allowance(wallet, spender)
        .then((approved) => approved.gte(biAmount))
}

function approveERC20ForEscrow(networkId, tokenAddress, amount) {
    const escrowContract = contracts.contract('escrowV1', networkId)
    const ERC20 = new ethers.Contract(tokenAddress, abi, ethersProvider())
    const signer = ethersProvider().getSigner()
    const ERC20signer = ERC20.connect(signer)
    return ERC20signer.approve(escrowContract.address, amount)
}

function approveERC20ForDisputer(networkId, tokenAddress, amount) {
    const disputerContract = contracts.contract('aragonCourtDisputerV1', networkId)
    const ERC20 = new ethers.Contract(tokenAddress, abi, ethersProvider())
    const signer = ethersProvider().getSigner()
    const ERC20signer = ERC20.connect(signer)
    return ERC20signer.approve(disputerContract.address, amount)
}

function getTestDaiAddress(networkId) {
    return networks[networkId]
}

function generateTestDaiToken(networkId, wallet, amount) {
    if (!isEthereumAddress(wallet)) return Promise.reject("use Ethereum address as wallet")
    const biAmount = parseAmount(amount)

    const ERC20 = new ethers.Contract(getTestDaiAddress(networkId), abi, ethersProvider())
    const signer = ethersProvider().getSigner()
    const ERC20signer = ERC20.connect(signer)
    return ERC20signer.generateTokens(wallet, biAmount)
}

function getAragonCourtDisputeFees(networkId) {
    const disputerContract = contracts.contract('aragonCourtDisputerV1', networkId)
    return disputerContract.ARBITER()
        .then((aragonCourtAddress) => {
            const aragonCourtContract = new ethers.Contract(aragonCourtAddress, [
                "function getDisputeFees() view returns (address recipient, address feeToken, uint256 feeAmount)"
            ], ethersProvider())
            return aragonCourtContract.getDisputeFees()
        })
}

function registerContract(networkId, cid, payer, payee, payerDelegate = null, payeeDelegate = null, milestones = []) {
    if (!payerDelegate) payerDelegate = payer
    if (!payeeDelegate) payeeDelegate = payee

    if (!isEthereumAddress(payer)) return Promise.reject("use Ethereum address as payer")
    if (!isEthereumAddress(payee)) return Promise.reject("use Ethereum address as payee")
    if (!isEthereumAddress(payerDelegate)) return Promise.reject("use Ethereum address as payerDelegate")
    if (!isEthereumAddress(payeeDelegate)) return Promise.reject("use Ethereum address as payeeDelegate")

    let cidBytes32
    try {
        cidBytes32 = parseCid(cid, "use v0 IPFS cid")

        if (milestones.length > 0) {
            milestones = milestones.map((param) => parseMilestoneParams(networkId, param))
        }
    } catch (e) {
        return Promise.reject(e)
    }

    const escrowContractSigner = escrowWithSigner(networkId)

    return escrowContractSigner.registerContract(
        cidBytes32,
        payer,
        payerDelegate,
        payee,
        payeeDelegate,
        milestones
    )
}

function registerMilestone(networkId, cid, index, paymentToken, treasury, payeeAccount, refundAccount, autoReleasedAt = 0, amount, amendmentCid = null) {
    let cidBytes32, amendmentCidBytes32
    try {
        cidBytes32 = parseCid(cid, "use v0 IPFS cid")

        let response = parseMilestoneParams(networkId, { paymentToken, treasury, payeeAccount, refundAccount, autoReleasedAt, amount })
        paymentToken = response.paymentToken
        treasury = response.treasury
        payeeAccount = response.payeeAccount
        refundAccount = response.refundAccount
        escrowDisputeManager = response.escrowDisputeManager
        autoReleasedAt = response.autoReleasedAt
        amount = response.amount

        // By default use contract cid for milestone
        amendmentCidBytes32 = parseCid(amendmentCid, "use v0 IPFS cid for amendment", cidBytes32)

        index = parseMilestoneIndex(index)
    } catch (e) {
        return Promise.reject(e)
    }

    const escrowContractSigner = escrowWithSigner(networkId)
    return escrowContractSigner.registerMilestone(
        cidBytes32,
        index,
        paymentToken,
        treasury,
        payeeAccount,
        refundAccount,
        escrowDisputeManager,
        autoReleasedAt,
        amount,
        amendmentCidBytes32
    )
}

function isApprovedContractVersion(networkId, cid, termsCid = null) {
    let cidBytes32, termsCidBytes32
    try {
        cidBytes32 = parseCid(cid, "use v0 IPFS cid")

        // By default use contract cid for terms
        termsCidBytes32 = parseCid(termsCid, "use v0 IPFS cid for milestone terms", cidBytes32)
    } catch (e) {
        return Promise.reject(e)
    }

    const escrowContract = contracts.contract('escrowV1', networkId)
    return escrowContract.isApprovedContractVersion(cidBytes32, termsCidBytes32)
}

function getMilestone(networkId, cid, index) {
    let cidBytes32
    try {
        cidBytes32 = parseCid(cid, "use v0 IPFS cid")
    } catch (e) {
        return Promise.reject(e)
    }

    const mid = genMid(cidBytes32, index)
    const escrowContract = contracts.contract('escrowV1', networkId)
    return escrowContract.milestones(mid)
}

function signAndApproveContractVersion(networkId, cid, termsCid = null) {
    let cidBytes32, termsCidBytes32
    try {
        cidBytes32 = parseCid(cid, "use v0 IPFS cid")

        // By default use contract cid for terms
        termsCidBytes32 = parseCid(termsCid, "use v0 IPFS cid for milestone terms", cidBytes32)
    } catch (e) {
        return Promise.reject(e)
    }

    const escrowContractSigner = escrowWithSigner(networkId)
    return escrowContractSigner.signAndApproveContractVersion(
        cidBytes32,
        termsCidBytes32
    )
}

function signMilestoneTermsForFunding(networkId, cid, milestoneTermsCid = null) {
    let cidBytes32, milestoneTermsCidBytes32
    try {
        cidBytes32 = parseCid(cid, "use v0 IPFS cid")

        // By default use contract cid for milestone
        milestoneTermsCidBytes32 = parseCid(milestoneTermsCid, "use v0 IPFS cid for milestone terms", cidBytes32)
    } catch (e) {
        return Promise.reject(e)
    }

    const escrowContractSigner = escrowWithSigner(networkId)
    return signContractTerms(
        currentChain(),
        escrowContractSigner.address,
        cidBytes32,
        cidBytes32,
        (message) => escrowContractSigner.signer.signMessage(ethers.utils.arrayify(message))
    )
}

function signAndFundMilestone(networkId, cid, index, milestoneTermsCid, amount, payeeSignature, payerSignature) {
    let cidBytes32, milestoneTermsCidBytes32
    try {
        cidBytes32 = parseCid(cid, "use v0 IPFS cid")

        // By default use contract cid for milestone
        milestoneTermsCidBytes32 = parseCid(milestoneTermsCid, "use v0 IPFS cid for milestone terms", cidBytes32)

        index = parseMilestoneIndex(index)
        amount = parseAmount(amount)
    } catch (e) {
        return Promise.reject(e)
    }

    const escrowContractSigner = escrowWithSigner(networkId)
    return escrowContractSigner.signAndFundMilestone(
        cidBytes32,
        index,
        milestoneTermsCidBytes32,
        amount,
        payeeSignature,
        payerSignature
    )
}

function fundMilestone(networkId, cid, index, amount) {
    let cidBytes32
    try {
        cidBytes32 = parseCid(cid, "use v0 IPFS cid")
        index = parseMilestoneIndex(index)
        amount = parseAmount(amount)
    } catch (e) {
        return Promise.reject(e)
    }

    const escrowContractSigner = escrowWithSigner(networkId)
    return escrowContractSigner.fundMilestone(
        cidBytes32,
        index,
        amount
    )
}

function releaseMilestone(networkId, cid, index, amount) {
    let cidBytes32
    try {
        cidBytes32 = parseCid(cid, "use v0 IPFS cid")
        index = parseMilestoneIndex(index)
        amount = parseAmount(amount)
    } catch (e) {
        return Promise.reject(e)
    }

    const escrowContractSigner = escrowWithSigner(networkId)
    return escrowContractSigner.releaseMilestone(
        cidBytes32,
        index,
        amount
    )
}

function cancelMilestone(networkId, cid, index, amount) {
    let cidBytes32
    try {
        cidBytes32 = parseCid(cid, "use v0 IPFS cid")
        index = parseMilestoneIndex(index)
        amount = parseAmount(amount)
    } catch (e) {
        return Promise.reject(e)
    }

    const escrowContractSigner = escrowWithSigner(networkId)
    return escrowContractSigner.cancelMilestone(
        cidBytes32,
        index,
        amount
    )
}

function signWithdrawApproval(networkId, cid, index, amount) {
    let cidBytes32
    try {
        cidBytes32 = parseCid(cid, "use v0 IPFS cid")
        index = parseMilestoneIndex(index)
        amount = parseAmount(amount)
    } catch (e) {
        return Promise.reject(e)
    }

    const escrowContractSigner = escrowWithSigner(networkId)
    return signMilestoneRelease(
        currentChain(),
        escrowContractSigner.address,
        cidBytes32,
        index,
        amount,
        (message) => escrowContractSigner.signer.signMessage(ethers.utils.arrayify(message))
    )
}

function withdrawMilestone(networkId, cid, index) {
    let cidBytes32
    try {
        cidBytes32 = parseCid(cid, "use v0 IPFS cid")
        index = parseMilestoneIndex(index)
    } catch (e) {
        return Promise.reject(e)
    }

    const escrowContractSigner = escrowWithSigner(networkId)
    return escrowContractSigner.withdrawMilestone(
        cidBytes32,
        index
    )
}

function withdrawPreApprovedMilestone(networkId, cid, index, amount, payerDelegateSignature) {
    let cidBytes32
    try {
        cidBytes32 = parseCid(cid, "use v0 IPFS cid")
        index = parseMilestoneIndex(index)
        amount = parseAmount(amount)
    } catch (e) {
        return Promise.reject(e)
    }

    const escrowContractSigner = escrowWithSigner(networkId)
    return escrowContractSigner.withdrawPreApprovedMilestone(
        cidBytes32,
        index,
        amount,
        payerDelegateSignature
    )
}

function signRefundApproval(networkId, cid, index, amount) {
    let cidBytes32
    try {
        cidBytes32 = parseCid(cid, "use v0 IPFS cid")
        index = parseMilestoneIndex(index)
        amount = parseAmount(amount)
    } catch (e) {
        return Promise.reject(e)
    }

    const escrowContractSigner = escrowWithSigner(networkId)
    return signMilestoneRefund(
        currentChain(),
        escrowContractSigner.address,
        cidBytes32,
        index,
        amount,
        (message) => escrowContractSigner.signer.signMessage(ethers.utils.arrayify(message))
    )
}

function refundMilestone(networkId, cid, index) {
    let cidBytes32
    try {
        cidBytes32 = parseCid(cid, "use v0 IPFS cid")
        index = parseMilestoneIndex(index)
    } catch (e) {
        return Promise.reject(e)
    }

    const escrowContractSigner = escrowWithSigner(networkId)
    return escrowContractSigner.refundMilestone(
        cidBytes32,
        index
    )
}

function refundPreApprovedMilestone(networkId, cid, index, amount, payeeDelegateSignature) {
    let cidBytes32
    try {
        cidBytes32 = parseCid(cid, "use v0 IPFS cid")
        index = parseMilestoneIndex(index)
        amount = parseAmount(amount)
    } catch (e) {
        return Promise.reject(e)
    }

    const escrowContractSigner = escrowWithSigner(networkId)
    return escrowContractSigner.refundPreApprovedMilestone(
        cidBytes32,
        index,
        amount,
        payeeDelegateSignature
    )
}

function proposeSettlement(networkId, cid, index, refundedPercent, releasedPercent, statement) {
    let cidBytes32, statementBytes32
    try {
        cidBytes32 = parseCid(cid, "use v0 IPFS cid")
        statementBytes32 = parseCid(statement, "use v0 IPFS cid")
        index = parseMilestoneIndex(index)
        const percents = parsePercents(refundedPercent, releasedPercent)
        refundedPercent = percents[0]
        releasedPercent = percents[1]
    } catch (e) {
        return Promise.reject(e)
    }

    const escrowContractSigner = escrowWithSigner(networkId)
    return escrowContractSigner.proposeSettlement(
        cidBytes32,
        index,
        refundedPercent,
        releasedPercent,
        statementBytes32
    )
}

function acceptSettlement(networkId, cid, index) {
    let cidBytes32
    try {
        cidBytes32 = parseCid(cid, "use v0 IPFS cid")
        index = parseMilestoneIndex(index)
    } catch (e) {
        return Promise.reject(e)
    }

    const escrowContractSigner = escrowWithSigner(networkId)
    return escrowContractSigner.acceptSettlement(
        cidBytes32,
        index
    )
}

function disputeSettlement(networkId, cid, index, ignoreCoverage = true) {
    let cidBytes32
    try {
        cidBytes32 = parseCid(cid, "use v0 IPFS cid")
        index = parseMilestoneIndex(index)
    } catch (e) {
        return Promise.reject(e)
    }

    const escrowContractSigner = escrowWithSigner(networkId)
    return escrowContractSigner.disputeSettlement(
        cidBytes32,
        index,
        ignoreCoverage
    )
}

function submitEvidence(networkId, cid, index, evidence) {
    let cidBytes32
    try {
        cidBytes32 = parseCid(cid, "use v0 IPFS cid")
        index = parseMilestoneIndex(index)
    } catch (e) {
        return Promise.reject(e)
    }

    let evidenceBytes, evidence32Bytes
    if (evidence.length === 46) {
        evidence32Bytes = parseCid(evidence, "use v0 IPFS cid")
        evidenceBytes = ethers.utils.solidityPack(["bytes2", "bytes32"], ["0x1220", evidence32Bytes])
    } else {
        evidenceBytes = ethers.utils.defaultAbiCoder.encode(["string"], [evidence])
    }

    const escrowContractSigner = escrowWithSigner(networkId)
    return escrowContractSigner.submitEvidence(
        cidBytes32,
        index,
        evidenceBytes
    )
}

function executeSettlement(networkId, cid, index) {
    let cidBytes32
    try {
        cidBytes32 = parseCid(cid, "use v0 IPFS cid")
        index = parseMilestoneIndex(index)
    } catch (e) {
        return Promise.reject(e)
    }

    const escrowContractSigner = escrowWithSigner(networkId)
    return escrowContractSigner.executeSettlement(
        cidBytes32,
        index
    )
}

export default {
    ERC20decimals,
    ERC20symbol,
    isApprovedERC20Amount,
    approveERC20ForEscrow,
    approveERC20ForDisputer,
    getTestDaiAddress,
    generateTestDaiToken,
    getAragonCourtDisputeFees,
    registerContract,
    registerMilestone,
    isApprovedContractVersion,
    getMilestone,
    signAndApproveContractVersion,
    signMilestoneTermsForFunding,
    fundMilestone,
    signAndFundMilestone,
    releaseMilestone,
    cancelMilestone,
    signWithdrawApproval,
    withdrawMilestone,
    withdrawPreApprovedMilestone,
    signRefundApproval,
    refundMilestone,
    refundPreApprovedMilestone,
    proposeSettlement,
    acceptSettlement,
    disputeSettlement,
    submitEvidence,
    executeSettlement
}