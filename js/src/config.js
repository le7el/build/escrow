import { ethers } from 'ethers'

let RPC_ENDPOINT = 'https://mainnet.infura.io/v3/51609976c26542e18e6cd119a1755fbf'

let defaultEthersProvider, defaultNetwork = 1 // Production

if (document.location.hostname.startsWith('demo') || document.location.search.startsWith('?rinkeby')) {
    defaultNetwork = 4 // Rinkeby
    RPC_ENDPOINT = 'https://rinkeby.infura.io/v3/51609976c26542e18e6cd119a1755fbf'
}

if (document.location.hostname === 'localhost' || document.location.hostname.startsWith('192.168')) {
    defaultNetwork = 5777 // Local ganache
    const host = document.location.hostname === 'localhost' ? '127.0.0.1' : document.location.hostname
    defaultEthersProvider = new ethers.providers.JsonRpcProvider(`http://${document.location.hostname}:8545`)
    RPC_ENDPOINT = `http://${host}:8545`
} else {
    defaultEthersProvider = new ethers.providers.JsonRpcProvider(RPC_ENDPOINT)
}

const isMetamask = () => {
    return localStorage.getItem('provider') !== 'walletconnect' &&
        typeof window.ethereum !== 'undefined' &&
        typeof window.ethereum.isMetaMask !== 'undefined' &&
        window.ethereum.isMetaMask === true
}

const ethersProvider = () => {
    if (isMetamask()) {
        return new ethers.providers.Web3Provider(window.ethereum, "any");
    } else {
        return defaultEthersProvider
    }
}

const currentChain = () => {
    return window.localStorage.getItem('selectedChain')
}

const currentNetwork = () => {
    const selectedNetwork = window.localStorage.getItem('selectedNetwork')
    return selectedNetwork || defaultNetwork
}

const currentAddress = () => {
    return window.localStorage.getItem('wallet')
}

export {
    RPC_ENDPOINT,
    ethersProvider,
    defaultEthersProvider,
    defaultNetwork,
    currentNetwork,
    currentChain,
    currentAddress
}