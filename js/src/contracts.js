import { ethersProvider } from './config'
import { ethers } from 'ethers'

import escrowV1Artifact from '../../build/contracts/EscrowV1.json'
import disputerV1Artifact from '../../build/contracts/AragonCourtDisputerV1.json'

let contracts = {}
let subscriptions = {}

const defaultLoader = (appNetworkId, provider, networks, abi) => {
    appNetworkId = `${appNetworkId}`
    let deployedNetwork = networks[appNetworkId === '1337' ? '5777' : appNetworkId]
    if (!deployedNetwork) {
        console.log('Unsupported network', appNetworkId)
        return null
    }

    return new ethers.Contract(deployedNetwork.address, abi, provider)
}

const lazyContract = (name, appNetworkId, provider, { networks, abi }, loader) => {
    const key = `${name}-${appNetworkId}`

    if (!loader) {
        loader = defaultLoader
    }

    if (!contracts[key]) {
        contracts[key] = loader(appNetworkId, provider, networks, abi)
        return contracts[key]
    } else {
        if (Promise.resolve(contracts[key]) === contracts[key]) {
            return contracts[key].then(c => c.connect(provider))
        } else {
            return contracts[key].connect(provider)
        }
    }
}

const contract = (contractName, appNetworkId) => {
    switch (contractName) {
        case 'escrowV1':
            return lazyContract(contractName, appNetworkId, ethersProvider(), escrowV1Artifact)

        case 'aragonCourtDisputerV1':
            return lazyContract(contractName, appNetworkId, ethersProvider(), disputerV1Artifact)

        default:
            return null
    }
}

const contractPromise = (contractName, appNetworkId) => {
    let name, arg1
    if (Array.isArray(contractName)) {
        [name, arg1] = contractName
        if (name) contractName = name
    }

    switch (contractName) {
        case 'escrowV1':
            return Promise.resolve(new ethers.Contract(arg1, escrowV1Artifact.abi, ethersProvider()))

        case 'aragonCourtDisputerV1':
            return Promise.resolve(new ethers.Contract(arg1, disputerV1Artifact.abi, ethersProvider()))

        default:
            return Promise.resolve(contract(contractName, appNetworkId))
    }
}

// Subscribe only once for the same event
const subOnceToContract = (contractName, appNetworkId, event, eventFilter, fn) => {
    const key = `${contractName}-${appNetworkId}-${event}`
    const c = contractPromise(contractName, appNetworkId)

    if (!subscriptions[key]) {
        subscriptions[key] = true
        return c.then(cnt => cnt.on(eventFilter, fn))
    } else {
        return c
    }
}

export default { contract, contractPromise, subOnceToContract }