import { utils } from 'ethers'

function encodeMessage(signature) {
    const sig = utils.splitSignature(utils.arrayify(signature))
    return utils.defaultAbiCoder.encode(
        ["uint8", "bytes32", "bytes32"], [sig.v, sig.r, sig.s],
    )
}

function getSignedContractDomain(chainId, verifyingContract) {
    return {
        name: "SignedContract",
        version: "v1",
        chainId,
        verifyingContract
    }
}

function getAmendmentDomain(chainId, verifyingContract) {
    return {
        name: "ApprovedAmendment",
        version: "v1",
        chainId,
        verifyingContract
    }
}

function getMReleaseDomain(chainId, verifyingContract) {
    return {
        name: "ReleasedMilestone",
        version: "v1",
        chainId,
        verifyingContract
    }
}

function getMRefundDomain(chainId, verifyingContract) {
    return {
        name: "RefundedMilestone",
        version: "v1",
        chainId,
        verifyingContract
    }
}

function signContractTerms(chainId, verifyingContract, cid, termsCid, signMessage) {
    const domain = getSignedContractDomain(chainId, verifyingContract)
    const message = utils.keccak256(
        utils.defaultAbiCoder.encode(
            ["bytes32", "bytes32", "bytes32"], [
                utils._TypedDataEncoder.hashDomain(
                    domain,
                ),
                cid,
                termsCid
            ],
        ),
    )
    return signMessage(message).then(encodeMessage)
}

function signContractAmendment(chainId, verifyingContract, cid, currentCid, amendmentCid, signMessage) {
    const domain = getAmendmentDomain(chainId, verifyingContract)
    const message = utils.keccak256(
        utils.defaultAbiCoder.encode(
            ["bytes32", "bytes32", "bytes32", "bytes32"], [
                utils._TypedDataEncoder.hashDomain(
                    domain,
                ),
                cid,
                currentCid,
                amendmentCid
            ],
        ),
    )
    return signMessage(message).then(encodeMessage)
}

function signMilestoneRelease(chainId, verifyingContract, cid, index, amount, signMessage) {
    const domain = getMReleaseDomain(chainId, verifyingContract)
    const message = utils.keccak256(
        utils.defaultAbiCoder.encode(
            ["bytes32", "bytes32", "uint16", "uint256"], [
                utils._TypedDataEncoder.hashDomain(
                    domain,
                ),
                cid,
                index,
                amount
            ],
        ),
    )
    return signMessage(message).then(encodeMessage)
}

function signMilestoneRefund(chainId, verifyingContract, cid, index, amount, signMessage) {
    const domain = getMRefundDomain(chainId, verifyingContract)
    const message = utils.keccak256(
        utils.defaultAbiCoder.encode(
            ["bytes32", "bytes32", "uint16", "uint256"], [
                utils._TypedDataEncoder.hashDomain(
                    domain,
                ),
                cid,
                index,
                amount
            ],
        ),
    )
    return signMessage(message).then(encodeMessage)
}

export default { signContractTerms, signContractAmendment, signMilestoneRelease, signMilestoneRefund }