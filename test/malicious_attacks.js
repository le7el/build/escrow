const { assert } = require('chai')
const truffleAssert = require('truffle-assertions')
const { GreetEscrow } = require('../js/dist/server')

const AragonProtocol = artifacts.require('AragonProtocolMock')
const EscrowDisputeManager = artifacts.require('AragonCourtDisputerV1')
const EscrowForAragon = artifacts.require('EscrowV1')
const PaymentToken = artifacts.require('PaymentTokenMock')
const MaliciousDisputer = artifacts.require('MaliciousDisputerMock')
const MaliciousToken = artifacts.require('MaliciousTokenMock')
const MaliciousReciever = artifacts.require('MaliciousTokenRecieverMock')

const RULE_PAYEE_WON = '3';

const ATK_CID1 = '0xbcdd5c0f6298bb7bc8f9c3f7888d641f4ed56bc64003f5376cbb5069c6a010b5'
const GOOD_CID1 = '0xbcdd5c0f6298bb7bc8f9c3f7888d641f4ed56bc64003f5376cbb5069c6a010b7'
const ATK_CID2 = '0xbcdd5c0f6298bb7bc8f9c3f7888d641f4ed56bc64003f5376cbb5069c6a010b9'
const GOOD_CID2 = '0xbcdd5c0f6298bb7bc8f9c3f7888d641f4ed56bc64003f5376cbb5069c6a01011'
const ATK_CID3 = '0xbcdd5c0f6298bb7bc8f9c3f7888d641f4ed56bc64003f5376cbb5069c6a01013'
const GOOD_CID3 = '0xbcdd5c0f6298bb7bc8f9c3f7888d641f4ed56bc64003f5376cbb5069c6a01015'
const ATK_CID4 = '0xbcdd5c0f6298bb7bc8f9c3f7888d641f4ed56bc64003f5376cbb5069c6a01017'
const GOOD_CID4 = '0xbcdd5c0f6298bb7bc8f9c3f7888d641f4ed56bc64003f5376cbb5069c6a01019'
const ATK_CID5 = '0xbcdd5c0f6298bb7bc8f9c3f7888d641f4ed56bc64003f5376cbb5069c6a01021'
const GOOD_CID5 = '0xbcdd5c0f6298bb7bc8f9c3f7888d641f4ed56bc64003f5376cbb5069c6a01023'
const ATK_CID6 = '0xbcdd5c0f6298bb7bc8f9c3f7888d641f4ed56bc64003f5376cbb5069c6a01025'
const GOOD_CID6 = '0xbcdd5c0f6298bb7bc8f9c3f7888d641f4ed56bc64003f5376cbb5069c6a01027'
const ATK_CID7 = '0xbcdd5c0f6298bb7bc8f9c3f7888d641f4ed56bc64003f5376cbb5069c6a01029'
const GOOD_CID7 = '0xbcdd5c0f6298bb7bc8f9c3f7888d641f4ed56bc64003f5376cbb5069c6a01031'
const ATK_CID8 = '0xbcdd5c0f6298bb7bc8f9c3f7888d641f4ed56bc64003f5376cbb5069c6a01033'
const GOOD_CID8 = '0xbcdd5c0f6298bb7bc8f9c3f7888d641f4ed56bc64003f5376cbb5069c6a01035'
const ATK_CID9 = '0xbcdd5c0f6298bb7bc8f9c3f7888d641f4ed56bc64003f5376cbb5069c6a01037'
const GOOD_CID9 = '0xbcdd5c0f6298bb7bc8f9c3f7888d641f4ed56bc64003f5376cbb5069c6a01039'
const ATK_CID10 = '0xbcdd5c0f6298bb7bc8f9c3f7888d641f4ed56bc64003f5376cbb5069c6a01041'
const GOOD_CID10 = '0xbcdd5c0f6298bb7bc8f9c3f7888d641f4ed56bc64003f5376cbb5069c6a01043'
const ATK_CID11 = '0xbcdd5c0f6298bb7bc8f9c3f7888d641f4ed56bc64003f5376cbb5069c6a01045'
const GOOD_CID11 = '0xbcdd5c0f6298bb7bc8f9c3f7888d641f4ed56bc64003f5376cbb5069c6a01047'
const STATEMENT_CID = '0xcf116662909ada21c1ce86fce1b5d388e2edcb08856004fe53c2d17632b85d73'

function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}

const genMid = GreetEscrow.util.genMid

// State persists sequencially for each test
contract('EscrowV1', accounts => {
    const PAYER = accounts[1]
    const PAYEE = accounts[2]
    const RANDOM = accounts[3]
    const ATTACKER = accounts[4]
    const ACCESSORY = accounts[5]

    it('should not allow re-entrancy in executeSettlement', async() => {
        const escrow = await EscrowForAragon.deployed()
        const attacker = await MaliciousDisputer.deployed()
        const paymentToken = await PaymentToken.deployed()
        let fundValue = '50000000'

        const milestoneParams1 = [{
            paymentToken: paymentToken.address,
            treasury: EscrowForAragon.address,
            payeeAccount: ATTACKER,
            refundAccount: attacker.address,
            escrowDisputeManager: MaliciousDisputer.address,
            autoReleasedAt: 0,
            amount: fundValue,
            parentIndex: 0
        }];

        const milestoneParams2 = [{
            paymentToken: paymentToken.address,
            treasury: EscrowForAragon.address,
            payeeAccount: PAYEE,
            refundAccount: PAYER,
            escrowDisputeManager: EscrowDisputeManager.address,
            autoReleasedAt: 0,
            amount: fundValue,
            parentIndex: 0
        }];

        await escrow.registerContract.sendTransaction(
            GOOD_CID1,
            PAYER,
            PAYER,
            PAYEE,
            PAYEE,
            milestoneParams2, {from: PAYER}
        );

        await escrow.registerContract.sendTransaction(
            ATK_CID1,
            attacker.address,
            attacker.address,
            ATTACKER,
            ATTACKER,
            milestoneParams1, { from: ATTACKER }
        );

        const victimSignature = await GreetEscrow.eip712.signContractTerms(1, EscrowForAragon.address, ATK_CID1, ATK_CID1, 
            (message) => web3.eth.sign(message.toString(), ATTACKER));
        const payeeSignature = await GreetEscrow.eip712.signContractTerms(1, EscrowForAragon.address, GOOD_CID1, GOOD_CID1, 
            (message) => web3.eth.sign(message.toString(), PAYEE));

        await attacker.initialize(escrow.address, paymentToken.address, 1, ATK_CID1);
        await paymentToken.faucet.sendTransaction({ from: PAYER });
        await paymentToken.transfer.sendTransaction(attacker.address, fundValue, { from: PAYER });

        // await attacker.initialize(escrow.address, pay.address, VICTIM, 1, ATK_CID1);
        await attacker.signAndFundMilestone(ATK_CID1, 1, ATK_CID1, fundValue, victimSignature, { from: RANDOM });
        await paymentToken.approve(escrow.address, fundValue, { from: PAYER });
        await escrow.signAndFundMilestone(GOOD_CID1, 1, GOOD_CID1, fundValue, payeeSignature, payeeSignature, { from: PAYER });
        
        await escrow.proposeSettlement.sendTransaction(ATK_CID1, 1, 50, 50, STATEMENT_CID, { from: ATTACKER });
        
        await truffleAssert.fails(
            escrow.executeSettlement.sendTransaction(ATK_CID1, 1, { from: RANDOM }),
            null,
            "ReentrancyGuard: reentrant call"
        )

        let finalBalance = await paymentToken.balanceOf(escrow.address);
        assert.equal(finalBalance.toNumber(), 100000000, "it should be equal to 2 * fundValue");
    });

    it('should not allow re-entrancy in withdrawMilestone', async() => {
        const escrow = await EscrowForAragon.deployed()
        const paymentToken = await MaliciousToken.deployed()
        const attacker = paymentToken
        let fundValue = '50000000'

        const milestoneParams1 = [{
            paymentToken: paymentToken.address,
            treasury: EscrowForAragon.address,
            payeeAccount: ATTACKER,
            refundAccount: attacker.address,
            escrowDisputeManager: EscrowDisputeManager.address,
            autoReleasedAt: 0,
            amount: fundValue,
            parentIndex: 0
        }];

        const milestoneParams2 = [{
            paymentToken: paymentToken.address,
            treasury: EscrowForAragon.address,
            payeeAccount: PAYEE,
            refundAccount: PAYER,
            escrowDisputeManager: EscrowDisputeManager.address,
            autoReleasedAt: 0,
            amount: fundValue,
            parentIndex: 0
        }];

        await escrow.registerContract.sendTransaction(
            GOOD_CID2,
            PAYER,
            PAYER,
            PAYEE,
            PAYEE,
            milestoneParams2, {from: PAYER}
        );

        await escrow.registerContract.sendTransaction(
            ATK_CID2,
            attacker.address,
            ACCESSORY,
            ATTACKER,
            ATTACKER,
            milestoneParams1, { from: ATTACKER }
        );

        const victimSignature = await GreetEscrow.eip712.signContractTerms(1, EscrowForAragon.address, ATK_CID2, ATK_CID2, 
            (message) => web3.eth.sign(message.toString(), ATTACKER));
        const payeeSignature = await GreetEscrow.eip712.signContractTerms(1, EscrowForAragon.address, GOOD_CID2, GOOD_CID2, 
            (message) => web3.eth.sign(message.toString(), PAYEE));

        await attacker.initialize(escrow.address, 1, ATK_CID2);
        await paymentToken.faucet.sendTransaction({ from: PAYER });
        await paymentToken.transfer.sendTransaction(attacker.address, fundValue, { from: PAYER });

        await attacker.signAndFundMilestone(ATK_CID2, 1, ATK_CID2, fundValue, victimSignature, { from: RANDOM });
        await paymentToken.approve(escrow.address, fundValue, { from: PAYER });
        await escrow.signAndFundMilestone(GOOD_CID2, 1, GOOD_CID2, fundValue, payeeSignature, payeeSignature, { from: PAYER });
        
        await escrow.releaseMilestone.sendTransaction(ATK_CID2, 1, fundValue, { from: ACCESSORY });
        await paymentToken.prepareAttack.sendTransaction(1, 0, 0x00, { from: ATTACKER });
        
        await truffleAssert.fails(
            escrow.withdrawMilestone.sendTransaction(ATK_CID2, 1, { from: RANDOM }),
            null,
            "ReentrancyGuard: reentrant call"
        )

        let finalBalance = await paymentToken.balanceOf(escrow.address);
        assert.equal(finalBalance.toNumber(), 100000000, "it should be equal to 2 * fundValue");
    });

    it('should not allow re-entrancy in withdrawPreApprovedMilestone', async() => {
        const escrow = await EscrowForAragon.deployed()
        const paymentToken = await MaliciousToken.deployed()
        const attacker = paymentToken
        let fundValue = '50000000'

        const milestoneParams1 = [{
            paymentToken: paymentToken.address,
            treasury: EscrowForAragon.address,
            payeeAccount: ATTACKER,
            refundAccount: ATTACKER,
            escrowDisputeManager: EscrowDisputeManager.address,
            autoReleasedAt: 0,
            amount: fundValue,
            parentIndex: 0
        }];

        const milestoneParams2 = [{
            paymentToken: paymentToken.address,
            treasury: EscrowForAragon.address,
            payeeAccount: PAYEE,
            refundAccount: PAYER,
            escrowDisputeManager: EscrowDisputeManager.address,
            autoReleasedAt: 0,
            amount: fundValue,
            parentIndex: 0
        }];

        await escrow.registerContract.sendTransaction(
            GOOD_CID4,
            PAYER,
            PAYER,
            PAYEE,
            PAYEE,
            milestoneParams2, {from: PAYER}
        );

        await escrow.registerContract.sendTransaction(
            ATK_CID4,
            attacker.address,
            ACCESSORY,
            ATTACKER,
            ATTACKER,
            milestoneParams1, { from: ATTACKER }
        );

        const victimSignature = await GreetEscrow.eip712.signContractTerms(1, EscrowForAragon.address, ATK_CID4, ATK_CID4, 
            (message) => web3.eth.sign(message.toString(), ATTACKER));
        const payeeSignature = await GreetEscrow.eip712.signContractTerms(1, EscrowForAragon.address, GOOD_CID4, GOOD_CID4, 
            (message) => web3.eth.sign(message.toString(), PAYEE));

        await attacker.initialize(escrow.address, 1, ATK_CID4);
        await paymentToken.faucet.sendTransaction({ from: PAYER });
        await paymentToken.transfer.sendTransaction(attacker.address, fundValue, { from: PAYER });

        await attacker.signAndFundMilestone(ATK_CID4, 1, ATK_CID4, fundValue, victimSignature, { from: RANDOM });
        await paymentToken.approve(escrow.address, fundValue, { from: PAYER });
        await escrow.signAndFundMilestone(GOOD_CID4, 1, GOOD_CID4, fundValue, payeeSignature, payeeSignature, { from: PAYER });
        
        const releaseSignature = await GreetEscrow.eip712.signMilestoneRelease(1, EscrowForAragon.address, ATK_CID4, 1, fundValue,
            (message) => web3.eth.sign(message.toString(), ACCESSORY));
        await paymentToken.prepareAttack.sendTransaction(2, fundValue, releaseSignature, { from: ATTACKER });
        
        await truffleAssert.fails(
            escrow.withdrawPreApprovedMilestone.sendTransaction(ATK_CID4, 1, fundValue, releaseSignature, { from: RANDOM }),
            null,
            "ReentrancyGuard: reentrant call"
        )

        let finalBalance = await paymentToken.balanceOf(escrow.address);
        assert.equal(finalBalance.toNumber(), 200000000, "it should be equal to 4 * fundValue");
    });

    it('should not allow re-entrancy in refundMilestone', async() => {
        const escrow = await EscrowForAragon.deployed()
        const paymentToken = await MaliciousToken.deployed()
        const attacker = paymentToken
        let fundValue = '50000000'

        const milestoneParams1 = [{
            paymentToken: paymentToken.address,
            treasury: EscrowForAragon.address,
            payeeAccount: ATTACKER,
            refundAccount: attacker.address,
            escrowDisputeManager: EscrowDisputeManager.address,
            autoReleasedAt: 0,
            amount: fundValue,
            parentIndex: 0
        }];

        const milestoneParams2 = [{
            paymentToken: paymentToken.address,
            treasury: EscrowForAragon.address,
            payeeAccount: PAYEE,
            refundAccount: PAYER,
            escrowDisputeManager: EscrowDisputeManager.address,
            autoReleasedAt: 0,
            amount: fundValue,
            parentIndex: 0
        }];

        await escrow.registerContract.sendTransaction(
            GOOD_CID3,
            PAYER,
            PAYER,
            PAYEE,
            PAYEE,
            milestoneParams2, {from: PAYER}
        );

        await escrow.registerContract.sendTransaction(
            ATK_CID3,
            attacker.address,
            ACCESSORY,
            ATTACKER,
            ATTACKER,
            milestoneParams1, { from: ATTACKER }
        );

        const victimSignature = await GreetEscrow.eip712.signContractTerms(1, EscrowForAragon.address, ATK_CID3, ATK_CID3, 
            (message) => web3.eth.sign(message.toString(), ATTACKER));
        const payeeSignature = await GreetEscrow.eip712.signContractTerms(1, EscrowForAragon.address, GOOD_CID3, GOOD_CID3, 
            (message) => web3.eth.sign(message.toString(), PAYEE));

        await attacker.initialize(escrow.address, 1, ATK_CID3);
        await paymentToken.faucet.sendTransaction({ from: PAYER });
        await paymentToken.transfer.sendTransaction(attacker.address, fundValue, { from: PAYER });
        
        await attacker.signAndFundMilestone(ATK_CID3, 1, ATK_CID3, fundValue, victimSignature, { from: RANDOM });
        await paymentToken.approve(escrow.address, fundValue, { from: PAYER });
        await escrow.signAndFundMilestone(GOOD_CID3, 1, GOOD_CID3, fundValue, payeeSignature, payeeSignature, { from: PAYER });

        await escrow.cancelMilestone.sendTransaction(ATK_CID3, 1, fundValue, { from: ATTACKER });
        await paymentToken.prepareAttack.sendTransaction(3, 0, 0x00, { from: ATTACKER });
        await truffleAssert.fails(
            escrow.refundMilestone.sendTransaction(ATK_CID3, 1, { from: RANDOM }),
            null,
            "ReentrancyGuard: reentrant call"
        )

        let finalBalance = await paymentToken.balanceOf(escrow.address);
        assert.equal(finalBalance.toNumber(), 300000000, "it should be equal to 6 * fundValue");
    });

    it('should not allow re-entrancy in refundPreApprovedMilestone', async() => {
        const escrow = await EscrowForAragon.deployed()
        const paymentToken = await MaliciousToken.deployed()
        const attacker = paymentToken
        let fundValue = '50000000'

        const milestoneParams1 = [{
            paymentToken: paymentToken.address,
            treasury: EscrowForAragon.address,
            payeeAccount: ATTACKER,
            refundAccount: ATTACKER,
            escrowDisputeManager: EscrowDisputeManager.address,
            autoReleasedAt: 0,
            amount: fundValue,
            parentIndex: 0
        }];

        const milestoneParams2 = [{
            paymentToken: paymentToken.address,
            treasury: EscrowForAragon.address,
            payeeAccount: PAYEE,
            refundAccount: PAYER,
            escrowDisputeManager: EscrowDisputeManager.address,
            autoReleasedAt: 0,
            amount: fundValue,
            parentIndex: 0
        }];

        await escrow.registerContract.sendTransaction(
            GOOD_CID5,
            PAYER,
            PAYER,
            PAYEE,
            PAYEE,
            milestoneParams2, {from: PAYER}
        );

        await escrow.registerContract.sendTransaction(
            ATK_CID5,
            attacker.address,
            ACCESSORY,
            ATTACKER,
            ATTACKER,
            milestoneParams1, { from: ATTACKER }
        );

        const victimSignature = await GreetEscrow.eip712.signContractTerms(1, EscrowForAragon.address, ATK_CID5, ATK_CID5, 
            (message) => web3.eth.sign(message.toString(), ATTACKER));
        const payeeSignature = await GreetEscrow.eip712.signContractTerms(1, EscrowForAragon.address, GOOD_CID5, GOOD_CID5, 
            (message) => web3.eth.sign(message.toString(), PAYEE));

        await attacker.initialize(escrow.address, 1, ATK_CID5);
        await paymentToken.faucet.sendTransaction({ from: PAYER });
        await paymentToken.transfer.sendTransaction(attacker.address, fundValue, { from: PAYER });
        
        await attacker.signAndFundMilestone(ATK_CID5, 1, ATK_CID5, fundValue, victimSignature, { from: RANDOM });
        await paymentToken.approve(escrow.address, fundValue, { from: PAYER });
        await escrow.signAndFundMilestone(GOOD_CID5, 1, GOOD_CID5, fundValue, payeeSignature, payeeSignature, { from: PAYER });

        const refundSignature = await GreetEscrow.eip712.signMilestoneRefund(1, EscrowForAragon.address, ATK_CID5, 1, fundValue,
            (message) => web3.eth.sign(message.toString(), ATTACKER));
        await paymentToken.prepareAttack.sendTransaction(4, fundValue, refundSignature, { from: ACCESSORY });

        await truffleAssert.fails(
            escrow.refundPreApprovedMilestone.sendTransaction(ATK_CID5, 1, fundValue, refundSignature, { from: RANDOM }),
            null,
            "ReentrancyGuard: reentrant call"
        )

        let finalBalance = await paymentToken.balanceOf(escrow.address);
        assert.equal(finalBalance.toNumber(), 400000000, "it should be equal to 8 * fundValue");
    });

    it('should not allow mixed re-entrancy', async() => {
        const escrow = await EscrowForAragon.deployed()
        const paymentToken = await MaliciousToken.deployed()
        const attacker = paymentToken
        let fundValue = '50000000'

        const milestoneParams1 = [{
            paymentToken: paymentToken.address,
            treasury: EscrowForAragon.address,
            payeeAccount: ATTACKER,
            refundAccount: ATTACKER,
            escrowDisputeManager: EscrowDisputeManager.address,
            autoReleasedAt: 0,
            amount: fundValue,
            parentIndex: 0
        }];

        const milestoneParams2 = [{
            paymentToken: paymentToken.address,
            treasury: EscrowForAragon.address,
            payeeAccount: PAYEE,
            refundAccount: PAYER,
            escrowDisputeManager: EscrowDisputeManager.address,
            autoReleasedAt: 0,
            amount: fundValue,
            parentIndex: 0
        }];

        await escrow.registerContract.sendTransaction(
            GOOD_CID6,
            PAYER,
            PAYER,
            PAYEE,
            PAYEE,
            milestoneParams2, {from: PAYER}
        );

        await escrow.registerContract.sendTransaction(
            ATK_CID6,
            attacker.address,
            ACCESSORY,
            ATTACKER,
            ATTACKER,
            milestoneParams1, { from: ATTACKER }
        );

        const victimSignature = await GreetEscrow.eip712.signContractTerms(1, EscrowForAragon.address, ATK_CID6, ATK_CID6, 
            (message) => web3.eth.sign(message.toString(), ATTACKER));
        const payeeSignature = await GreetEscrow.eip712.signContractTerms(1, EscrowForAragon.address, GOOD_CID6, GOOD_CID6, 
            (message) => web3.eth.sign(message.toString(), PAYEE));

        await attacker.initialize(escrow.address, 1, ATK_CID6);
        await paymentToken.faucet.sendTransaction({ from: PAYER });
        await paymentToken.transfer.sendTransaction(attacker.address, fundValue, { from: PAYER });

        await attacker.signAndFundMilestone(ATK_CID6, 1, ATK_CID6, fundValue, victimSignature, { from: RANDOM });
        await paymentToken.approve(escrow.address, fundValue, { from: PAYER });
        await escrow.signAndFundMilestone(GOOD_CID6, 1, GOOD_CID6, fundValue, payeeSignature, payeeSignature, { from: PAYER });
        
        await escrow.releaseMilestone.sendTransaction(ATK_CID6, 1, fundValue, { from: ACCESSORY });
        const releaseSignature = await GreetEscrow.eip712.signMilestoneRelease(1, EscrowForAragon.address, ATK_CID6, 1, fundValue,
            (message) => web3.eth.sign(message.toString(), ACCESSORY));
        await paymentToken.prepareAttack.sendTransaction(1, fundValue, releaseSignature, { from: ATTACKER });
        
        await truffleAssert.fails(
            escrow.withdrawPreApprovedMilestone.sendTransaction(ATK_CID6, 1, fundValue, releaseSignature, { from: RANDOM }),
            null,
            "ReentrancyGuard: reentrant call"
        )

        let finalBalance = await paymentToken.balanceOf(escrow.address);
        assert.equal(finalBalance.toNumber(), 500000000, "it should be equal to 10 * fundValue");
    });

    it('should not allow re-entrancy in executeSettlement with malicious reciever', async() => {
        const protocol = await AragonProtocol.deployed()
        const escrow = await EscrowForAragon.deployed()
        const attacker = await MaliciousReciever.deployed()
        const paymentToken = await MaliciousToken.deployed()
        const courtFeeToken = await PaymentToken.deployed()
        let fundValue = '50000000'

        const milestoneParams1 = [{
            paymentToken: paymentToken.address,
            treasury: EscrowForAragon.address,
            payeeAccount: MaliciousReciever.address,
            refundAccount: MaliciousReciever.address,
            escrowDisputeManager: EscrowDisputeManager.address,
            autoReleasedAt: 0,
            amount: fundValue,
            parentIndex: 0
        }];

        const milestoneParams2 = [{
            paymentToken: paymentToken.address,
            treasury: EscrowForAragon.address,
            payeeAccount: PAYEE,
            refundAccount: PAYER,
            escrowDisputeManager: EscrowDisputeManager.address,
            autoReleasedAt: 0,
            amount: fundValue,
            parentIndex: 0
        }];

        await escrow.registerContract.sendTransaction(
            GOOD_CID7,
            PAYER,
            PAYER,
            PAYEE,
            PAYEE,
            milestoneParams2, { from: PAYER }
        );

        await escrow.registerContract.sendTransaction(
            ATK_CID7,
            attacker.address,
            ACCESSORY,
            ATTACKER,
            ATTACKER,
            milestoneParams1, { from: ATTACKER }
        );

        const victimSignature = await GreetEscrow.eip712.signContractTerms(1, EscrowForAragon.address, ATK_CID7, ATK_CID7, 
            (message) => web3.eth.sign(message.toString(), ATTACKER));
        const payeeSignature = await GreetEscrow.eip712.signContractTerms(1, EscrowForAragon.address, GOOD_CID7, GOOD_CID7, 
            (message) => web3.eth.sign(message.toString(), PAYEE));

        await paymentToken.initialize(escrow.address, 1, ATK_CID6); // to reset possible token level attack
        await attacker.initialize(escrow.address, paymentToken.address, 1, ATK_CID7);
        await paymentToken.faucet.sendTransaction({ from: PAYER });
        await paymentToken.transfer.sendTransaction(attacker.address, fundValue, { from: PAYER });

        await attacker.signAndFundMilestone(ATK_CID7, 1, ATK_CID7, fundValue, victimSignature, { from: RANDOM });
        await paymentToken.approve(escrow.address, fundValue, { from: PAYER });
        await escrow.signAndFundMilestone(GOOD_CID7, 1, GOOD_CID7, fundValue, payeeSignature, payeeSignature, { from: PAYER });
        
        let initBalance = await paymentToken.balanceOf(escrow.address);
        assert.equal(initBalance.toNumber(), 600000000, "it should be equal to 12 * fundValue");

        await escrow.proposeSettlement.sendTransaction(ATK_CID7, 1, 50, 50, STATEMENT_CID, { from: ATTACKER });
        await sleep(2000) // should pass at least 1 second from first suggestion
        await courtFeeToken.faucet.sendTransaction({ from: ACCESSORY });
        await courtFeeToken.approve(EscrowDisputeManager.address, '100000000', { from: ACCESSORY });
        await escrow.disputeSettlement.sendTransaction(ATK_CID7, 1, true, { from: ACCESSORY });
        await protocol.setRulingForDispute(1, RULE_PAYEE_WON, { from: RANDOM }) // dispute indexation starts from 1
        
        await attacker.prepareAttack.sendTransaction(5, 0, 0x00, { from: ATTACKER });
        await escrow.executeSettlement.sendTransaction(ATK_CID7, 1, { from: RANDOM });

        const milestone1 = await escrow.milestones.call(genMid(ATK_CID7, 1), { from: RANDOM })
        assert.equal(milestone1.amount.toString(), '50000000', "invalid sum")
        assert.equal(milestone1.fundedAmount.toString(), '50000000', "should be 50")
        assert.equal(milestone1.releasedAmount.toString(), '25000000', "should be 25")
        assert.equal(milestone1.refundedAmount.toString(), '25000000', "should be 25")

        let finalBalance = await paymentToken.balanceOf(escrow.address);
        assert.equal(finalBalance.toNumber(), 600000000, "it should be equal to 12 * fundValue");
    });

    it('should not allow re-entrancy in withdrawMilestone with malicious reciever', async() => {
        const escrow = await EscrowForAragon.deployed()
        const attacker = await MaliciousReciever.deployed()
        const paymentToken = await MaliciousToken.deployed()
        let fundValue = '50000000'

        const milestoneParams1 = [{
            paymentToken: paymentToken.address,
            treasury: EscrowForAragon.address,
            payeeAccount: attacker.address,
            refundAccount: attacker.address,
            escrowDisputeManager: EscrowDisputeManager.address,
            autoReleasedAt: 0,
            amount: fundValue,
            parentIndex: 0
        }];

        const milestoneParams2 = [{
            paymentToken: paymentToken.address,
            treasury: EscrowForAragon.address,
            payeeAccount: PAYEE,
            refundAccount: PAYER,
            escrowDisputeManager: EscrowDisputeManager.address,
            autoReleasedAt: 0,
            amount: fundValue,
            parentIndex: 0
        }];

        await escrow.registerContract.sendTransaction(
            GOOD_CID8,
            PAYER,
            PAYER,
            PAYEE,
            PAYEE,
            milestoneParams2, { from: PAYER }
        );

        await escrow.registerContract.sendTransaction(
            ATK_CID8,
            attacker.address,
            ACCESSORY,
            ATTACKER,
            ATTACKER,
            milestoneParams1, { from: ATTACKER }
        );

        const victimSignature = await GreetEscrow.eip712.signContractTerms(1, EscrowForAragon.address, ATK_CID8, ATK_CID8, 
            (message) => web3.eth.sign(message.toString(), ATTACKER));
        const payeeSignature = await GreetEscrow.eip712.signContractTerms(1, EscrowForAragon.address, GOOD_CID8, GOOD_CID8, 
            (message) => web3.eth.sign(message.toString(), PAYEE));

        await attacker.initialize(escrow.address, paymentToken.address, 1, ATK_CID8);
        await paymentToken.faucet.sendTransaction({ from: PAYER });
        await paymentToken.transfer.sendTransaction(attacker.address, fundValue, { from: PAYER });

        await attacker.signAndFundMilestone(ATK_CID8, 1, ATK_CID8, fundValue, victimSignature, { from: RANDOM });
        await paymentToken.approve(escrow.address, fundValue, { from: PAYER });
        await escrow.signAndFundMilestone(GOOD_CID8, 1, GOOD_CID8, fundValue, payeeSignature, payeeSignature, { from: PAYER });
        
        await escrow.releaseMilestone.sendTransaction(ATK_CID8, 1, fundValue, { from: ACCESSORY });
        await attacker.prepareAttack.sendTransaction(1, 0, 0x00, { from: ATTACKER });
        await truffleAssert.fails(
            escrow.withdrawMilestone.sendTransaction(ATK_CID8, 1, { from: RANDOM }),
            null,
            "ReentrancyGuard: reentrant call"
        )

        let finalBalance = await paymentToken.balanceOf(escrow.address);
        assert.equal(finalBalance.toNumber(), 700000000, "it should be equal to 14 * fundValue");
    });

    it('should not allow mixed re-entrancy in withdrawPreApprovedMilestone with malicious reciever', async() => {
        const escrow = await EscrowForAragon.deployed()
        const attacker = await MaliciousReciever.deployed()
        const paymentToken = await MaliciousToken.deployed()
        let fundValue = '50000000'

        const milestoneParams1 = [{
            paymentToken: paymentToken.address,
            treasury: EscrowForAragon.address,
            payeeAccount: attacker.address,
            refundAccount: attacker.address,
            escrowDisputeManager: EscrowDisputeManager.address,
            autoReleasedAt: 0,
            amount: fundValue,
            parentIndex: 0
        }];

        const milestoneParams2 = [{
            paymentToken: paymentToken.address,
            treasury: EscrowForAragon.address,
            payeeAccount: PAYEE,
            refundAccount: PAYER,
            escrowDisputeManager: EscrowDisputeManager.address,
            autoReleasedAt: 0,
            amount: fundValue,
            parentIndex: 0
        }];

        await escrow.registerContract.sendTransaction(
            GOOD_CID9,
            PAYER,
            PAYER,
            PAYEE,
            PAYEE,
            milestoneParams2, { from: PAYER }
        );

        await escrow.registerContract.sendTransaction(
            ATK_CID9,
            attacker.address,
            ACCESSORY,
            ATTACKER,
            ATTACKER,
            milestoneParams1, { from: ATTACKER }
        );

        const victimSignature = await GreetEscrow.eip712.signContractTerms(1, EscrowForAragon.address, ATK_CID9, ATK_CID9, 
            (message) => web3.eth.sign(message.toString(), ATTACKER));
        const payeeSignature = await GreetEscrow.eip712.signContractTerms(1, EscrowForAragon.address, GOOD_CID9, GOOD_CID9, 
            (message) => web3.eth.sign(message.toString(), PAYEE));

        await attacker.initialize(escrow.address, paymentToken.address, 1, ATK_CID9);
        await paymentToken.faucet.sendTransaction({ from: PAYER });
        await paymentToken.transfer.sendTransaction(attacker.address, fundValue, { from: PAYER });

        await attacker.signAndFundMilestone(ATK_CID9, 1, ATK_CID9, fundValue, victimSignature, { from: RANDOM });
        await paymentToken.approve(escrow.address, fundValue, { from: PAYER });
        await escrow.signAndFundMilestone(GOOD_CID9, 1, GOOD_CID9, fundValue, payeeSignature, payeeSignature, { from: PAYER });
        
        await escrow.releaseMilestone.sendTransaction(ATK_CID9, 1, fundValue, { from: ACCESSORY });
        const releaseSignature = await GreetEscrow.eip712.signMilestoneRelease(1, EscrowForAragon.address, ATK_CID9, 1, fundValue,
            (message) => web3.eth.sign(message.toString(), ACCESSORY));
        await attacker.prepareAttack.sendTransaction(2, fundValue, releaseSignature, { from: ATTACKER });
        await truffleAssert.fails(
            escrow.withdrawMilestone.sendTransaction(ATK_CID9, 1, { from: RANDOM }),
            null,
            "ReentrancyGuard: reentrant call"
        )

        let finalBalance = await paymentToken.balanceOf(escrow.address);
        assert.equal(finalBalance.toNumber(), 800000000, "it should be equal to 16 * fundValue");
    });

    it('should not allow re-entrancy in double funding with malicious sender', async() => {
        const escrow = await EscrowForAragon.deployed()
        const attacker = await MaliciousReciever.deployed()
        const paymentToken = await MaliciousToken.deployed()
        let fundValue = '50000000'
        let halfFundValue = '20000000'

        const milestoneParams1 = [{
            paymentToken: paymentToken.address,
            treasury: EscrowForAragon.address,
            payeeAccount: attacker.address,
            refundAccount: attacker.address,
            escrowDisputeManager: EscrowDisputeManager.address,
            autoReleasedAt: 0,
            amount: fundValue,
            parentIndex: 0
        }];

        const milestoneParams2 = [{
            paymentToken: paymentToken.address,
            treasury: EscrowForAragon.address,
            payeeAccount: PAYEE,
            refundAccount: PAYER,
            escrowDisputeManager: EscrowDisputeManager.address,
            autoReleasedAt: 0,
            amount: fundValue,
            parentIndex: 0
        }];

        await escrow.registerContract.sendTransaction(
            GOOD_CID10,
            PAYER,
            PAYER,
            PAYEE,
            PAYEE,
            milestoneParams2, { from: PAYER }
        );

        await escrow.registerContract.sendTransaction(
            ATK_CID10,
            attacker.address,
            ACCESSORY,
            ATTACKER,
            ATTACKER,
            milestoneParams1, { from: ATTACKER }
        );

        const victimSignature = await GreetEscrow.eip712.signContractTerms(1, EscrowForAragon.address, ATK_CID10, ATK_CID10, 
            (message) => web3.eth.sign(message.toString(), ATTACKER));
        const payeeSignature = await GreetEscrow.eip712.signContractTerms(1, EscrowForAragon.address, GOOD_CID10, GOOD_CID10, 
            (message) => web3.eth.sign(message.toString(), PAYEE));

        await attacker.initialize(escrow.address, paymentToken.address, 1, ATK_CID10);
        await paymentToken.faucet.sendTransaction({ from: PAYER });
        await paymentToken.transfer.sendTransaction(attacker.address, fundValue, { from: PAYER });
        await paymentToken.transfer.sendTransaction(attacker.address, halfFundValue, { from: PAYER });

        await paymentToken.approve(escrow.address, fundValue, { from: PAYER });
        await escrow.signAndFundMilestone(GOOD_CID10, 1, GOOD_CID10, fundValue, payeeSignature, payeeSignature, { from: PAYER });
        
        let balance1 = await paymentToken.balanceOf(attacker.address);
        await attacker.signAndFundMilestone(ATK_CID10, 1, ATK_CID10, 10000000, victimSignature, { from: RANDOM });
        await attacker.prepareAttack.sendTransaction(6, halfFundValue, 0x00, { from: ATTACKER });
        await truffleAssert.fails(
            attacker.fundMilestone(ATK_CID10, 1, halfFundValue, { from: RANDOM }),
            null,
            "SafeERC20: low-level call failed"
        )
        let balance2 = await paymentToken.balanceOf(attacker.address);
        assert.equal(balance1.sub(balance2).toNumber(), 10000000, "invalid balance");
        
        const milestone1 = await escrow.milestones.call(genMid(ATK_CID10, 1), { from: RANDOM })
        assert.equal(milestone1.amount.toString(), '50000000', "invalid sum")
        assert.equal(milestone1.fundedAmount.toString(), '10000000', "should be 10")
        
        let finalBalance = await paymentToken.balanceOf(escrow.address);
        assert.equal(finalBalance.toNumber(), 860000000, "it should be equal to 15.2 * fundValue");
    });

    it('should not allow malicious resolutions in executeSettlement', async() => {
        const escrow = await EscrowForAragon.deployed()
        const attacker = await MaliciousDisputer.deployed()
        const paymentToken = await PaymentToken.deployed()
        let fundValue = '50000000'

        const milestoneParams1 = [{
            paymentToken: paymentToken.address,
            treasury: EscrowForAragon.address,
            payeeAccount: ATTACKER,
            refundAccount: attacker.address,
            escrowDisputeManager: MaliciousDisputer.address,
            autoReleasedAt: 0,
            amount: fundValue,
            parentIndex: 0
        }];

        const milestoneParams2 = [{
            paymentToken: paymentToken.address,
            treasury: EscrowForAragon.address,
            payeeAccount: PAYEE,
            refundAccount: PAYER,
            escrowDisputeManager: EscrowDisputeManager.address,
            autoReleasedAt: 0,
            amount: fundValue,
            parentIndex: 0
        }];

        await escrow.registerContract.sendTransaction(
            GOOD_CID11,
            PAYER,
            PAYER,
            PAYEE,
            PAYEE,
            milestoneParams2, {from: PAYER}
        );

        await escrow.registerContract.sendTransaction(
            ATK_CID11,
            attacker.address,
            attacker.address,
            ATTACKER,
            ATTACKER,
            milestoneParams1, { from: ATTACKER }
        );

        const victimSignature = await GreetEscrow.eip712.signContractTerms(1, EscrowForAragon.address, ATK_CID11, ATK_CID11, 
            (message) => web3.eth.sign(message.toString(), ATTACKER));
        const payeeSignature = await GreetEscrow.eip712.signContractTerms(1, EscrowForAragon.address, GOOD_CID11, GOOD_CID11, 
            (message) => web3.eth.sign(message.toString(), PAYEE));

        await attacker.initialize(escrow.address, paymentToken.address, 1, ATK_CID11);
        await paymentToken.faucet.sendTransaction({ from: PAYER });
        await paymentToken.transfer.sendTransaction(attacker.address, fundValue, { from: PAYER });

        await attacker.signAndFundMilestone(ATK_CID11, 1, ATK_CID11, fundValue, victimSignature, { from: RANDOM });
        await paymentToken.approve(escrow.address, fundValue, { from: PAYER });
        await escrow.signAndFundMilestone(GOOD_CID11, 1, GOOD_CID11, fundValue, payeeSignature, payeeSignature, { from: PAYER });
        
        await escrow.proposeSettlement.sendTransaction(ATK_CID11, 1, 50, 50, STATEMENT_CID, { from: ATTACKER });
        
        await attacker.noAttack();
        await truffleAssert.fails(
            escrow.executeSettlement.sendTransaction(ATK_CID11, 1, { from: RANDOM }),
            null,
            "100% required"
        )

        let finalBalance = await paymentToken.balanceOf(escrow.address);
        assert.equal(finalBalance.toNumber(), 200000000, "it should be equal to 4 * fundValue");
    });
})