const { assert } = require('chai')
const truffleAssert = require('truffle-assertions')
const { GreetEscrow } = require('../js/dist/server')

const EscrowDisputeManager = artifacts.require('AragonCourtDisputerV1')
const EscrowForAragon = artifacts.require('EscrowV1')
const PaymentToken = artifacts.require('PaymentTokenMock')

const ZERO_BYTES32 = '0x0000000000000000000000000000000000000000000000000000000000000000'
const ZERO_ADDRESS = '0x0000000000000000000000000000000000000000'
const DEAD_ADDRESS = '0x000000000000000000000000000000000000dead'

const CONTRACT_CID = '0x91dc4f7a9e354387bed9f3ddbf89c9f4959c9d206cb1c794af73960816dc50a9' //'QmYA2fn8cMbVWo4v95RwcwJVyQsNtnEwHerfWR8UNtEwoE'
const AMENDMENT1_CID = '0xcf116662909ada21c1ce86fce1b5d388e2edcb08856004fe53c2d17632b85d73' //'QmcGxVTu2BDV5bU9AeLxhTMS6aXDWSL5dN9HDSxNgV5T2J'
const AMENDMENT2_CID = '0xcf6a209306b7d4bd9041a8d1d4b25577d503b7bc06313166d8dbd5ffe237a9ee' //'QmcJJxmvWZP7Xvz8ZccYUi6wMMFPETX7Xe16AyRPYAUj2d'
const AMENDMENT3_CID = '0x29f2d17be6139079dc48696d1f582a8530eb9805b561eda517e22a892c7e3f1f' //'QmRAQB6YaCyidP37UdDnjFY5vQuiBrcqdyoW1CuDgwxkD4'
const AMENDMENT4_CID = '0x5260e5bd0791d38438b891514d6158b5306884e9b7f4ef4b6433c6943285776c' //'QmTtDqWzo179ujTXU7pf2PodLNjpcpQQCXhkiQXi6wZvKd'
const AMENDMENT5_CID = '0x91dc4f7a9e354387bed9f3ddbf89c9f4959c9d206cb1c794af73960816dc50a9' //'QmYA2fn8cMbVWo4v95RwcwJVyQsNtnEwHerfWR8UNtEwoE'

function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}

const genMid = GreetEscrow.util.genMid

// State persists sequencially for each test
contract('EscrowV1', accounts => {
    const PAYER = accounts[1]
    const PAYEE = accounts[2]
    const RANDOM = accounts[3]
    const ACC1 = accounts[4]
    const ACC2 = accounts[5]
    const ACC3 = accounts[6]

    it('should allow registration of new contract', async() => {
        const escrow = await EscrowForAragon.deployed()
        const milestone_params = [{
            paymentToken: PaymentToken.address,
            treasury: EscrowForAragon.address,
            payeeAccount: PAYEE,
            refundAccount: PAYER,
            escrowDisputeManager: EscrowDisputeManager.address,
            autoReleasedAt: 0,
            amount: '100000000',
            parentIndex: 0
        }, {
            paymentToken: PaymentToken.address,
            treasury: EscrowForAragon.address,
            payeeAccount: PAYEE,
            refundAccount: PAYER,
            escrowDisputeManager: EscrowDisputeManager.address,
            autoReleasedAt: 0,
            amount: '100000000',
            parentIndex: 0
        }]
        await escrow.registerContract.sendTransaction(
            CONTRACT_CID,
            PAYER,
            ZERO_ADDRESS,
            PAYEE,
            DEAD_ADDRESS,
            milestone_params, { from: RANDOM }
        )
    })

    it('should allow amendments from payer', async() => {
        const escrow = await EscrowForAragon.deployed()
        assert.equal(await escrow.getLatestApprovedContractVersion(CONTRACT_CID), ZERO_BYTES32, "Invalid terms")

        await escrow.signAndProposeContractVersion(CONTRACT_CID, AMENDMENT1_CID, { from: PAYER })
        await escrow.signAndApproveContractVersion(CONTRACT_CID, AMENDMENT1_CID, { from: PAYEE })

        assert.equal(await escrow.getLatestApprovedContractVersion(CONTRACT_CID), AMENDMENT1_CID, "Invalid terms")
        assert.equal(await escrow.isApprovedContractVersion(CONTRACT_CID, AMENDMENT1_CID), true, "Not approved")
    })

    it('should allow amendments from payee', async() => {
        const escrow = await EscrowForAragon.deployed()
        assert.equal(await escrow.getLatestApprovedContractVersion(CONTRACT_CID), AMENDMENT1_CID, "Invalid terms")

        await sleep(1000) // should pass at least 1 second from previous amendment
        await escrow.signAndProposeContractVersion(CONTRACT_CID, AMENDMENT2_CID, { from: PAYEE })
        await escrow.signAndApproveContractVersion(CONTRACT_CID, AMENDMENT2_CID, { from: PAYER })

        assert.equal(await escrow.getLatestApprovedContractVersion(CONTRACT_CID), AMENDMENT2_CID, "Invalid terms")
        assert.equal(await escrow.isApprovedContractVersion(CONTRACT_CID, AMENDMENT2_CID), true, "Not approved")
    })

    it('should not allow legacy amendments', async() => {
        const escrow = await EscrowForAragon.deployed()
        assert.equal(await escrow.getLatestApprovedContractVersion(CONTRACT_CID), AMENDMENT2_CID, "Invalid terms")

        await sleep(1000) // should pass at least 1 second from previous amendment
        await escrow.signAndProposeContractVersion(CONTRACT_CID, AMENDMENT3_CID, { from: PAYEE })
        await escrow.signAndProposeContractVersion(CONTRACT_CID, AMENDMENT4_CID, { from: PAYEE })
        await escrow.signAndApproveContractVersion(CONTRACT_CID, AMENDMENT4_CID, { from: PAYER })
        await truffleAssert.fails(
            escrow.signAndApproveContractVersion.sendTransaction(CONTRACT_CID, AMENDMENT3_CID, { from: PAYER }),
            null,
            "Not final amendment"
        )

        assert.equal(await escrow.getLatestApprovedContractVersion(CONTRACT_CID), AMENDMENT4_CID, "Invalid terms")
    })

    it('should not allow amendment approval from non-validator', async() => {
        const escrow = await EscrowForAragon.deployed()
        assert.equal(await escrow.getLatestApprovedContractVersion(CONTRACT_CID), AMENDMENT4_CID, "Invalid terms")

        await escrow.signAndProposeContractVersion(CONTRACT_CID, AMENDMENT3_CID, { from: PAYEE })
        await truffleAssert.fails(
            escrow.signAndApproveContractVersion.sendTransaction(CONTRACT_CID, AMENDMENT3_CID, { from: PAYEE }),
            null,
            "Not a validator"
        )

        assert.equal(await escrow.getLatestApprovedContractVersion(CONTRACT_CID), AMENDMENT4_CID, "Invalid terms")
    })

    it('should not allow amendment approval for already approved amendments', async() => {
        const escrow = await EscrowForAragon.deployed()
        assert.equal(await escrow.getLatestApprovedContractVersion(CONTRACT_CID), AMENDMENT4_CID, "Invalid terms")

        await escrow.signAndProposeContractVersion(CONTRACT_CID, AMENDMENT2_CID, { from: PAYEE })
        await truffleAssert.fails(
            escrow.signAndApproveContractVersion.sendTransaction(CONTRACT_CID, AMENDMENT2_CID, { from: PAYER }),
            null,
            "Amendment exist"
        )

        assert.equal(await escrow.getLatestApprovedContractVersion(CONTRACT_CID), AMENDMENT4_CID, "Invalid terms")
    })

    it('should allow pre-approved amendments from anyone', async() => {
        const escrow = await EscrowForAragon.deployed()
        assert.equal(await escrow.getLatestApprovedContractVersion(CONTRACT_CID), AMENDMENT4_CID, "Invalid terms")

        const payeeSignature = await GreetEscrow.eip712.signContractAmendment(
            1,
            EscrowForAragon.address,
            CONTRACT_CID,
            AMENDMENT4_CID,
            AMENDMENT5_CID,
            (message) => web3.eth.sign(message.toString(), PAYEE)
        )

        const payerSignature = await GreetEscrow.eip712.signContractAmendment(
            1,
            EscrowForAragon.address,
            CONTRACT_CID,
            AMENDMENT4_CID,
            AMENDMENT5_CID,
            (message) => web3.eth.sign(message.toString(), PAYER)
        )

        await escrow.preApprovedAmendment(CONTRACT_CID, AMENDMENT5_CID, payeeSignature, payerSignature, { from: RANDOM })

        assert.equal(await escrow.getLatestApprovedContractVersion(CONTRACT_CID), AMENDMENT5_CID, "Invalid terms")
        assert.equal(await escrow.isApprovedContractVersion(CONTRACT_CID, AMENDMENT5_CID), true, "Not approved")
    })

    it('should allow pre-approved amendment from payer', async() => {
        const escrow = await EscrowForAragon.deployed()
        assert.equal(await escrow.getLatestApprovedContractVersion(CONTRACT_CID), AMENDMENT5_CID, "Invalid terms")

        const payeeSignature = await GreetEscrow.eip712.signContractAmendment(
            1,
            EscrowForAragon.address,
            CONTRACT_CID,
            AMENDMENT5_CID,
            AMENDMENT3_CID,
            (message) => web3.eth.sign(message.toString(), PAYEE)
        )

        await escrow.preApprovedAmendment(CONTRACT_CID, AMENDMENT3_CID, payeeSignature, payeeSignature, { from: PAYER })

        assert.equal(await escrow.getLatestApprovedContractVersion(CONTRACT_CID), AMENDMENT3_CID, "Invalid terms")
        assert.equal(await escrow.isApprovedContractVersion(CONTRACT_CID, AMENDMENT3_CID), true, "Not approved")
    })

    it('should allow pre-approved amendment from payee', async() => {
        const escrow = await EscrowForAragon.deployed()
        assert.equal(await escrow.getLatestApprovedContractVersion(CONTRACT_CID), AMENDMENT3_CID, "Invalid terms")

        const payerSignature = await GreetEscrow.eip712.signContractAmendment(
            1,
            EscrowForAragon.address,
            CONTRACT_CID,
            AMENDMENT3_CID,
            AMENDMENT4_CID,
            (message) => web3.eth.sign(message.toString(), PAYER)
        )

        await escrow.preApprovedAmendment(CONTRACT_CID, AMENDMENT4_CID, payerSignature, payerSignature, { from: PAYEE })

        assert.equal(await escrow.getLatestApprovedContractVersion(CONTRACT_CID), AMENDMENT4_CID, "Invalid terms")
        assert.equal(await escrow.isApprovedContractVersion(CONTRACT_CID, AMENDMENT4_CID), true, "Not approved")
    })

    it('should allow milestone settlements', async() => {
        const escrow = await EscrowForAragon.deployed()
        await escrow.signAndProposeMilestoneSettlement(
            CONTRACT_CID,
            2,
            AMENDMENT1_CID,
            ACC1,
            ACC2,
            ACC3,
            1000,
            '50000000',
            '26000000',
            '24000000', { from: PAYEE }
        )
        await escrow.signApproveAndExecuteMilestoneSettlement(CONTRACT_CID, 2, 1, { from: PAYER })

        const milestone1 = await escrow.milestones.call(genMid(CONTRACT_CID, 2), { from: RANDOM })
        assert.equal(milestone1.amount.toString(), '50000000', "invalid sum")
        assert.equal(milestone1.autoReleasedAt.toString(), '1000', "invalid delivery deadline")
        assert.equal(milestone1.payeeAccount.toString(), ACC1, "should be ACC1")
        assert.equal(milestone1.refundAccount.toString(), ACC2, "should be ACC2")
        assert.equal(milestone1.escrowDisputeManager.toString(), ACC3, "should be ACC3")
        assert.equal(milestone1.refundedAmount.toString(), '26000000', "should be 26")
        assert.equal(milestone1.releasedAmount.toString(), '24000000', "should be 24")
        assert.equal(milestone1.claimedAmount.toString(), '0', "should be 0")

        assert.equal(await escrow.getLatestApprovedContractVersion(CONTRACT_CID), AMENDMENT1_CID, "Invalid terms")
    })

    it('should allow milestone settlements with partial changes', async() => {
        const escrow = await EscrowForAragon.deployed()
        await escrow.signAndProposeMilestoneSettlement(
            CONTRACT_CID,
            2,
            ZERO_BYTES32,
            ZERO_ADDRESS,
            ZERO_ADDRESS,
            ZERO_ADDRESS,
            1000,
            '100000000',
            '36000000',
            '34000000', { from: PAYER }
        )
        await escrow.signApproveAndExecuteMilestoneSettlement(CONTRACT_CID, 2, 2, { from: PAYEE })

        const milestone1 = await escrow.milestones.call(genMid(CONTRACT_CID, 2), { from: RANDOM })
        assert.equal(milestone1.amount.toString(), '100000000', "invalid sum")
        assert.equal(milestone1.autoReleasedAt.toString(), '1000', "invalid delivery deadline")
        assert.equal(milestone1.payeeAccount.toString(), ACC1, "should be ACC1")
        assert.equal(milestone1.refundAccount.toString(), ACC2, "should be ACC2")
        assert.equal(milestone1.escrowDisputeManager.toString(), ACC3, "should be ACC3")
        assert.equal(milestone1.refundedAmount.toString(), '36000000', "should be 36")
        assert.equal(milestone1.releasedAmount.toString(), '34000000', "should be 34")
        assert.equal(milestone1.claimedAmount.toString(), '0', "should be 0")

        assert.equal(await escrow.getLatestApprovedContractVersion(CONTRACT_CID), AMENDMENT1_CID, "Invalid terms")
    })

    it('should not allow milestone settlements with approval from invalid party', async() => {
        const escrow = await EscrowForAragon.deployed()
        await escrow.signAndProposeMilestoneSettlement(
            CONTRACT_CID,
            2,
            ZERO_BYTES32,
            ZERO_ADDRESS,
            ZERO_ADDRESS,
            ZERO_ADDRESS,
            1000,
            '200000000',
            '50000000',
            '34000000', { from: PAYER }
        )

        await truffleAssert.fails(
            escrow.signApproveAndExecuteMilestoneSettlement(CONTRACT_CID, 2, 3, { from: PAYER }),
            null,
            "Not a validator"
        )
    })

    it('should not allow milestone settlements with approval for invalid revision (2 instead of 3)', async() => {
        const escrow = await EscrowForAragon.deployed()
        await escrow.signAndProposeMilestoneSettlement(
            CONTRACT_CID,
            2,
            ZERO_BYTES32,
            ZERO_ADDRESS,
            ZERO_ADDRESS,
            ZERO_ADDRESS,
            1000,
            '200000000',
            '50000000',
            '34000000', { from: PAYER }
        )

        await truffleAssert.fails(
            escrow.signApproveAndExecuteMilestoneSettlement(CONTRACT_CID, 2, 2, { from: PAYEE }),
            null,
            "Not a validator"
        )
    })

    it('should not allow milestone settlements with overfunding', async() => {
        const escrow = await EscrowForAragon.deployed()
        const paymentToken = await PaymentToken.deployed()
        const payeeSignature = await GreetEscrow.eip712.signContractTerms(
            1,
            EscrowForAragon.address,
            CONTRACT_CID,
            AMENDMENT1_CID,
            (message) => web3.eth.sign(message.toString(), PAYEE)
        )
        await paymentToken.approve.sendTransaction(EscrowForAragon.address, '50000000', { from: PAYER })
        await escrow.signAndFundMilestone(CONTRACT_CID, 2, AMENDMENT1_CID, '50000000', payeeSignature, '0x00', { from: PAYER })
        const milestone1 = await escrow.milestones.call(genMid(CONTRACT_CID, 2), { from: RANDOM })
        assert.equal(milestone1.fundedAmount.toString(), '50000000', "should be 50")

        await escrow.signAndProposeMilestoneSettlement(
            CONTRACT_CID,
            2,
            ZERO_BYTES32,
            ZERO_ADDRESS,
            ZERO_ADDRESS,
            ZERO_ADDRESS,
            1000,
            '0',
            '40000000',
            '0', { from: PAYER }
        )

        await truffleAssert.fails(
            escrow.signApproveAndExecuteMilestoneSettlement(CONTRACT_CID, 2, 3, { from: PAYEE }),
            null,
            "Overfunded milestone"
        )
    })

    it('should allow milestone settlements with overfunding if refund / release covers the margin', async() => {
        const escrow = await EscrowForAragon.deployed()

        await escrow.signAndProposeMilestoneSettlement(
            CONTRACT_CID,
            2,
            ZERO_BYTES32,
            ZERO_ADDRESS,
            ZERO_ADDRESS,
            ZERO_ADDRESS,
            1000,
            '0',
            '27000000',
            '23000000', { from: PAYER }
        )
        await escrow.signApproveAndExecuteMilestoneSettlement(CONTRACT_CID, 2, 3, { from: PAYEE })

        milestone1 = await escrow.milestones.call(genMid(CONTRACT_CID, 2), { from: RANDOM })
        assert.equal(milestone1.amount.toString(), '0', "invalid sum")
        assert.equal(milestone1.autoReleasedAt.toString(), '1000', "invalid delivery deadline")
        assert.equal(milestone1.payeeAccount.toString(), ACC1, "should be ACC1")
        assert.equal(milestone1.refundAccount.toString(), ACC2, "should be ACC2")
        assert.equal(milestone1.escrowDisputeManager.toString(), ACC3, "should be ACC3")
        assert.equal(milestone1.refundedAmount.toString(), '27000000', "should be 27")
        assert.equal(milestone1.releasedAmount.toString(), '23000000', "should be 23")
        assert.equal(milestone1.fundedAmount.toString(), '50000000', "should be 50")
        assert.equal(milestone1.claimedAmount.toString(), '0', "should be 0")
    })
})