// SPDX-License-Identifier: MPL-2.0

pragma solidity >=0.8.4 <0.9.0;

import "../../node_modules/@openzeppelin/contracts/token/ERC20/IERC20.sol";

/** 
 * @title Mockup for Aragon court, including dispute manager
 */
contract AragonProtocolMock {
    struct Dispute {
        address subject;
        uint256 possibleRulings;
        uint256 timestamp;
        uint256 ruling;
        uint256 lastEvidenceIndex;
        bool applied;
        bytes metadata;
    }

    struct Evidence {
        address subject;
        uint256 timestamp;
        bytes evidence;
    }

    IERC20 public immutable FEE_TOKEN;
    uint256 public immutable FEE_AMOUNT;

    uint256 disputeIndex;
    mapping (uint256 => Dispute) disputes;
    mapping (uint256 => mapping(uint256 => Evidence)) evidences;

    constructor(address _feeToken, uint256 _feeAmount) {
        FEE_TOKEN = IERC20(_feeToken);
        FEE_AMOUNT = _feeAmount;
    }

    function createDispute(uint256 _possibleRulings, bytes calldata _metadata) external returns (uint256) {
        require(FEE_TOKEN.transferFrom(msg.sender, address(this), FEE_AMOUNT), "Fee payment failed");

        disputeIndex += 1;
        disputes[disputeIndex] = Dispute({
            subject: msg.sender,
            possibleRulings: _possibleRulings,
            timestamp: block.timestamp,
            ruling: 0,
            lastEvidenceIndex: 1,
            applied: false,
            metadata: _metadata
        });
        return disputeIndex;
    }

    function submitEvidence(uint256 _disputeId, address _submitter, bytes calldata _evidence) external {
        uint256 _lastEvidenceIndex = disputes[_disputeId].lastEvidenceIndex;
        require(_lastEvidenceIndex > 0, "No dispute");

        disputes[_disputeId].lastEvidenceIndex = _lastEvidenceIndex + 1;
        evidences[_disputeId][_lastEvidenceIndex + 1] = Evidence({
            subject: _submitter,
            timestamp: block.timestamp,
            evidence: _evidence
        });
    }

    function rule(uint256 _disputeId) external returns (address subject, uint256 ruling) {
        uint256 _ruling = disputes[_disputeId].ruling;
        require(_ruling > 0, "Not ruled");
        disputes[_disputeId].applied = true; // Used only to make it non-view as in interface
        return (disputes[_disputeId].subject, _ruling);
    }

    function getDisputeFees() external view returns (address recipient, IERC20 feeToken, uint256 feeAmount) {
        return (address(this), FEE_TOKEN, FEE_AMOUNT);
    }

    // Not needed in mock, but required for IAragonCourt interface
    function closeEvidencePeriod(uint256 _disputeId) external {

    }

    function setRulingForDispute(uint256 _disputeId, uint256 _ruling) external {
        uint256 _lastEvidenceIndex = disputes[_disputeId].lastEvidenceIndex;
        require(_lastEvidenceIndex > 0, "No dispute");

        disputes[_disputeId].ruling = _ruling;
    } 
}