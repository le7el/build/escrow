// SPDX-License-Identifier: MPL-2.0

pragma solidity >=0.8.4 <0.9.0;

import "../interfaces/IEscrow.sol";
import "../../node_modules/@openzeppelin/contracts/utils/introspection/IERC1820Registry.sol";
import "../../node_modules/@openzeppelin/contracts/token/ERC20/IERC20.sol";
import "../../node_modules/@openzeppelin/contracts/token/ERC777/IERC777Recipient.sol";
import "../../node_modules/@openzeppelin/contracts/token/ERC777/IERC777Sender.sol";

contract MaliciousTokenRecieverMock is IERC777Recipient, IERC777Sender {
    IEscrow public escrow;
    IERC20 public token;
    bytes32 public cid;
    uint16 public index;
    bool public initialized = false;
    bool attackDone = true;
    uint256 attackType = 0;
    bytes attackSign;
    uint attackAmount;

    IERC1820Registry private _erc1820 = IERC1820Registry(0x1820a4B7618BdE71Dce8cdc73aAB6C95905faD24);
    bytes32 constant private TOKENS_RECIPIENT_INTERFACE_HASH = keccak256("ERC777TokensRecipient");
    bytes32 constant public TOKENS_SENDER_INTERFACE_HASH = keccak256("ERC777TokensSender");

    event TokenRecieved(
        address operator,
        address from,
        address to,
        uint256 amount,
        bytes userData,
        bytes operatorData
    );

    event TokenSent(
        address operator,
        address from,
        address to,
        uint256 amount,
        bytes userData,
        bytes operatorData
    );

    event AttackReceived();

    constructor() {
        _erc1820.setInterfaceImplementer(address(this), TOKENS_RECIPIENT_INTERFACE_HASH, address(this));
        _erc1820.setInterfaceImplementer(address(this), TOKENS_SENDER_INTERFACE_HASH, address(this));
    }

    function initialize(address _escrow, address _token, uint16 _index, bytes32 _cid) external {
        escrow = IEscrow(_escrow);
        token = IERC20(_token);
        index = _index;
        cid = _cid;
        initialized = true;
        attackDone = true;
    }

    function prepareAttack(uint256 _type, uint _amount, bytes calldata _sign) public {
        attackDone = false;
        attackType = _type;
        attackSign = _sign;
        attackAmount = _amount;
    }

    function tokensReceived(
        address operator,
        address from,
        address to,
        uint256 amount,
        bytes calldata userData,
        bytes calldata operatorData
    ) external override {
        emit TokenRecieved(
            operator,
            from,
            to,
            amount,
            userData,
            operatorData
        );
        
        if (!attackDone && attackType > 0) {
            attackDone = true;
            emit AttackReceived();
            if (attackType == 1) escrow.withdrawMilestone(cid, index);
            if (attackType == 2) escrow.withdrawPreApprovedMilestone(cid, index, attackAmount, attackSign);
            if (attackType == 3) escrow.refundMilestone(cid, index);
            if (attackType == 4) escrow.refundPreApprovedMilestone(cid, index, attackAmount, attackSign);
            if (attackType == 5) escrow.executeSettlement(cid, index);
        }
    }

    function tokensToSend(
        address operator,
        address from,
        address to,
        uint256 amount,
        bytes calldata userData,
        bytes calldata operatorData
    ) external override {
        emit TokenSent(
            operator,
            from,
            to,
            amount,
            userData,
            operatorData
        );
        
        if (!attackDone && attackType > 0) {
            attackDone = true;
            if (attackType == 6) {
                escrow.fundMilestone(cid, index, attackAmount);
                revert();
            }
        }
    }

    function signAndFundMilestone(bytes32 id, uint16 ind, bytes32 terms, uint amount, bytes calldata sig) external {
        token.approve(address(escrow), amount);
        escrow.signAndFundMilestone(id, ind, terms, amount, sig, "");
    }

    function fundMilestone(bytes32 id, uint16 ind, uint amount) external {
        token.approve(address(escrow), amount);
        escrow.fundMilestone(id, ind, amount);
    }
}
