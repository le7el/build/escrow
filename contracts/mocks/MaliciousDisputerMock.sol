// SPDX-License-Identifier: MPL-2.0

pragma solidity >=0.8.4 <0.9.0;

import "../interfaces/IEscrowDisputeManager.sol";
import "../interfaces/IEscrow.sol";
import "../../node_modules/@openzeppelin/contracts/token/ERC20/IERC20.sol";

contract MaliciousDisputerMock is IEscrowDisputeManager {
    IEscrow public escrow;
    IERC20 public token;
    bytes32 public cid;
    uint16 public index;
    bool public initialized = false;
    bool attackDone = false;

    event NoWarning(
        address _addr1,
        address _addr2,
        address _addr3,
        uint16 _i1,
        bytes32 _bin1,
        bytes32 _bin2,
        uint _i2,
        uint _i3,
        bool _b1,
        bool _b2
    );

    constructor() {}

    function initialize(address _escrow, address _token, uint16 _index, bytes32 _cid) external {
        escrow = IEscrow(_escrow);
        token = IERC20(_token);
        index = _index;
        cid = _cid;
        initialized = true;
    }

    function noAttack() external {
        attackDone = true;
    }

    function proposeSettlement(
        bytes32 _cid,
        uint16 _index,
        address _plaintiff,
        address _payer,
        address _payee,
        uint _refundedPercent,
        uint _releasedPercent,
        bytes32 _statement
    ) external override {
        emit NoWarning(_plaintiff, _payer, _payee, _index, _cid, _statement, _refundedPercent, _releasedPercent, false, false);
        return;
    }

    function acceptSettlement(bytes32 _cid, uint16 _index, uint256 _ruling) external override {
        emit NoWarning(address(0), address(0), address(0), _index, _cid, bytes32(0), _ruling, 0, false, false);
        return;
    }

    function disputeSettlement(
        address _feePayer,
        bytes32 _cid,
        uint16 _index,
        bytes32 _termsCid,
        bool _ignoreCoverage,
        bool _multiMilestone
    ) external override {
        emit NoWarning(_feePayer, address(0), address(0), _index, _cid, _termsCid, 0, 0, _ignoreCoverage, _multiMilestone);
        return;
    }

    function executeSettlement(bytes32 _cid, uint16 _index, bytes32 _mid) external override returns(uint256, uint256, uint256) {
        emit NoWarning(address(0), address(0), address(0), _index, _cid, _mid, 0, 0, false, false);
        if (!initialized) {
            return (3, 150, 50);
        }

        if (!attackDone) {
            attackDone = true;
            escrow.executeSettlement(cid, index);
        }

        return (3, 150, 50);
    }
    
    function getSettlementByRuling(bytes32 _mid, uint256 _ruling) external override returns(uint256, uint256, uint256) {
        emit NoWarning(address(0), address(0), address(0), 0, bytes32(0), _mid, _ruling, 0, false, false);
        return (3, 50, 50);
    }
    
    function submitEvidence(address _from, string memory _label, bytes32 _cid, uint16 _index, bytes calldata _evidence) external override {}

    function ruleDispute(bytes32 _cid, uint16 _index, bytes32 _mid) external override returns(uint256) {
        emit NoWarning(address(0), address(0), address(0), _index, _cid, _mid, 0, 0, false, false);
        return 3;
    }
    
    function resolutions(bytes32 _mid) external pure override returns(uint256) {
        _mid = bytes32(0);
        return 3;
    }
    function hasSettlementDispute(bytes32 _mid) external pure override returns(bool) {
        _mid = bytes32(0);
        return true;
    }
    function ARBITER() external pure override returns(address) {
        return address(0);
    }

    function signAndFundMilestone(bytes32 id, uint16 ind, bytes32 terms, uint amount, bytes calldata sig) external {
        token.approve(address(escrow), amount);
        escrow.signAndFundMilestone(id, ind, terms, amount, sig, "");
    }
}