// SPDX-License-Identifier: MPL-2.0

pragma solidity >=0.8.4 <0.9.0;

import "../../node_modules/@openzeppelin/contracts/token/ERC20/IERC20.sol";
import "../interfaces/IRegistry.sol";

/** 
 * @title Mockup for treasury module.
 */
contract TreasuryMock {
    string private constant ERROR_FUNDING = "Funding failed";
    string private constant ERROR_BALANCE = "Balance exceeded";
    string private constant ERROR_NOT_ESCROW = "Not an escrow";

    IRegistry public immutable TRUSTED_REGISTRY;

    // Operator -> Token -> Balance
    mapping(address => mapping(address => uint)) public balances;

    event Deposit(
        address indexed operator,
        bytes32 indexed cid,
        address indexed token,
        address from,
        address to,
        uint amount
    );

    event Withdraw(
        address indexed operator,
        bytes32 indexed cid,
        address indexed token,
        address to,
        uint amount
    );

    /**
     * @dev Can only be a registered escrow contract.
     */
    modifier isEscrow() {
        require(TRUSTED_REGISTRY.escrowContracts(msg.sender), ERROR_NOT_ESCROW);
        _;
    }

    /**
     * @dev Single registry is used to store contract data from different versions of escrow contracts.
     *
     * @param _registry Address of universal registry of all contracts.
     */
    constructor(address _registry) {
        TRUSTED_REGISTRY = IRegistry(_registry);
    }

    /**
     * @dev Save claim in treasury module for custody.
     *
     * @param _termsCid Contract's IPFS cid.
     * @param _fromAccount Party which pays for the contract or on behalf of which the funding was done.
     * @param _toAccount Delegate who can release or dispute contract on behalf of payer.
     * @param _token Party which recieves the payment.
     * @param _amount Delegate who can refund or dispute contract on behalf of payee.
     * @return true is claim was successfuly registered, false otherwise.
     */
    function registerClaim(
      bytes32 _termsCid,
      address _fromAccount,
      address _toAccount,
      address _token,
      uint _amount
    ) external isEscrow() returns(bool) {
        require(IERC20(_token).transferFrom(msg.sender, address(this), _amount), ERROR_FUNDING);
        balances[msg.sender][_token] += _amount;

        emit Deposit(
            msg.sender,
            _termsCid,
            _token,
            _fromAccount,
            _toAccount,
            _amount
        );

        return true;
    }
    
    /**
     * @dev Withdraw from treasury module.
     *
     * @param _termsCid Contract's IPFS cid.
     * @param _toAccount Delegate who can release or dispute contract on behalf of payer.
     * @param _token Party which recieves the payment.
     * @param _amount Delegate who can refund or dispute contract on behalf of payee.
     * @return true if amount was withdrawn.
     */
    function requestWithdraw(
      bytes32 _termsCid,
      address _toAccount,
      address _token,
      uint _amount
    ) external isEscrow() returns(bool) {
        uint _balance = balances[msg.sender][_token];
        require(_balance >= _amount, ERROR_BALANCE);
        balances[msg.sender][_token] = _balance - _amount;

        require(IERC20(_token).transfer(_toAccount, _amount), ERROR_FUNDING);

        emit Withdraw(
            msg.sender,
            _termsCid,
            _token,
            _toAccount,
            _amount
        );

        return true;
    }
}