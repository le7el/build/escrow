// SPDX-License-Identifier: MPL-2.0

pragma solidity >=0.8.4 <0.9.0;

import "../../node_modules/@openzeppelin/contracts/access/Ownable.sol";
import "../../node_modules/@openzeppelin/contracts/token/ERC20/IERC20.sol";
import "../interfaces/IRegistry.sol";

/** 
 * @title Mockup for insurance module.
 */
contract InsuranceManagerMock is Ownable {
    string private constant ERROR_FUNDING = "Funding failed";
    string private constant ERROR_NOT_DISPUTER = "Not a disputer";

    mapping (bytes32 => mapping (address => uint)) coverages;
    mapping (address => bool) registeredDisputers;

    event UsedCoverage(
        address indexed operator,
        bytes32 indexed cid,
        address indexed token,
        uint coveredAmount
    );

    /**
     * @dev Can only be a registered disputer contract.
     */
    modifier isDisputer() {
        require(registeredDisputers[msg.sender], ERROR_NOT_DISPUTER);
        _;
    }

    constructor() {
    }

    /**
     * @dev Register insurance for specific contract.
     *
     * @param _cid Contract's IPFS cid.
     * @param _feeToken ERC20 token used to pay the fees.
     * @param _feeAmount Amount of ERC20 to pay fees.
     */
    function registerCoverage(bytes32 _cid, address _feeToken, uint _feeAmount) external onlyOwner {
        coverages[_cid][_feeToken] = _feeAmount;
    }

    /**
     * @dev Check if specific contract has insurance coverage.
     *
     * @param _cid Contract's IPFS cid.
     * @param _feeToken ERC20 token used to pay the fees.
     * @param _feeAmount Amount of ERC20 to pay fees.
     * @return covered and uncovered amounts.
     */
    function getCoverage(bytes32 _cid, address _feeToken, uint _feeAmount) public view returns (uint, uint) {
        uint _covered = coverages[_cid][_feeToken];
        if (_covered >= _feeAmount) {
            return (0, _feeAmount);
        } else if (_covered > 0) {
            return (_feeAmount - _covered, _covered);
        } else {
            return (_feeAmount, 0);
        }
    }

    /**
     * @dev Register coverage usage for contract and fund the sender's escrow disputer.
     *
     * @param _cid Contract's IPFS cid.
     * @param _feeToken ERC20 token used to pay the fees.
     * @param _feeAmount Amount of ERC20 to pay fees.
     */
    function useCoverage(bytes32 _cid, address _feeToken, uint _feeAmount) external isDisputer returns (bool) {
        require(IERC20(_feeToken).transfer(msg.sender, _feeAmount), ERROR_FUNDING);
        emit UsedCoverage(
            msg.sender,
            _cid,
            _feeToken,
            _feeAmount
        );
        return true;
    }

    /** 
     * @dev Register disputer smart-contract.
     *
     * @param _disputerContract Enable / disable disputer smart-contract.
     * @param _status true to enable, false to disable.
     */
    function toggleContractRegistration(address _disputerContract, bool _status) external onlyOwner {
        registeredDisputers[_disputerContract] = _status;
    }
}