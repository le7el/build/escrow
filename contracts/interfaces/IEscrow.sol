// SPDX-License-Identifier: MPL-2.0

pragma solidity >=0.8.4 <0.9.0;

interface IEscrow {
    function executeSettlement(bytes32 _cid, uint16 _index) external;
    function fundMilestone(bytes32 _cid, uint16 _index, uint _amountToFund) external;
    function releaseMilestone(bytes32 _cid, uint16 _index, uint _amount) external;
    function withdrawMilestone(bytes32 _cid, uint16 _index) external;
    function withdrawPreApprovedMilestone(bytes32 _cid, uint16 _index, uint _amount, bytes calldata _payerDelegateSignature) external;
    function refundMilestone(bytes32 _cid, uint16 _index) external;
    function refundPreApprovedMilestone(bytes32 _cid, uint16 _index, uint _amount, bytes calldata _payeeDelegateSignature) external;
    function signAndFundMilestone(
        bytes32 _cid,
        uint16 _index,
        bytes32 _termsCid,
        uint _amountToFund,
        bytes calldata _payeeSignature,
        bytes calldata _payerSignature
    ) external;
}