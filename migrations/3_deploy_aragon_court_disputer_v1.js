const RegistryContract = artifacts.require("Registry")
const AragonCourtDisputer = artifacts.require("AragonCourtDisputerV1")
const AragonProtocolMockContract = artifacts.require("AragonProtocolMock")
const InsuranceManagerMock = artifacts.require("InsuranceManagerMock")
const MaliciousDisputerMockContract = artifacts.require("MaliciousDisputerMock")

module.exports = async(deployer, network, accounts) => {
    let ARBITER = accounts[5];
    let INSURANCE_MANAGER = null;
    let SETTLEMENT_DELAY = 604800;

    try {
        const registryContract = await RegistryContract.deployed()

        if (network === 'live_mainnet') {
            ARBITER = '0xFb072baA713B01cE944A0515c3e1e98170977dAF'
            //SETTLEMENT_DELAY = 10080 // 2.8 hours
        } else if (network === 'live_rinkeby') {
            ARBITER = '0x9c003eC97676c30a041f128D671b3Db2f790c3E7' // '0x94903D782eaa1eC440989102Edd348044355aF15'
            //DISPUTE_MANAGER = '0xC0e572c9Ceb06AdFE2d754AFae16C9e2037D3B7f' // '0x7853584d41392bC4F7C82B0D53Fe46A7C0ce6353'
            SETTLEMENT_DELAY = 600 // 10 minutes
        } else if (network === 'develop' || network === 'soliditycoverage') {
            const aragonProtocolMockContract = await AragonProtocolMockContract.deployed()
            ARBITER = aragonProtocolMockContract.address
            SETTLEMENT_DELAY = 1 // For tests
            
            const insuranceManagerMockContract = await deployer.deploy(InsuranceManagerMock)
            INSURANCE_MANAGER = insuranceManagerMockContract.address

            await deployer.deploy(MaliciousDisputerMockContract)
        }

        const disputerContract = await deployer.deploy(AragonCourtDisputer, registryContract.address, ARBITER, SETTLEMENT_DELAY)
        if (INSURANCE_MANAGER) {
            await registryContract.setInsuranceManager(INSURANCE_MANAGER)
            const insuranceManagerMockContract = await InsuranceManagerMock.deployed()
            await insuranceManagerMockContract.toggleContractRegistration(disputerContract.address, true)
        }
    } catch (err) {
        console.error(err)
    }
}