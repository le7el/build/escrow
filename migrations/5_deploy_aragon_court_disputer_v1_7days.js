const RegistryContract = artifacts.require("Registry")
const AragonCourtDisputer = artifacts.require("AragonCourtDisputerV1")

module.exports = async(deployer, network, accounts) => {
    let ARBITER = '0xFb072baA713B01cE944A0515c3e1e98170977dAF';
    let SETTLEMENT_DELAY = 604800; // 7 days

    if (network !== 'live_mainnet') {
        return;
    }

    try {
        const registryContract = await RegistryContract.deployed()
        await deployer.deploy(AragonCourtDisputer, registryContract.address, ARBITER, SETTLEMENT_DELAY)
    } catch (err) {
        console.error(err)
    }
}